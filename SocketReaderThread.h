// ---------------------------------------------------------------------------

#ifndef SocketReaderThreadH
#define SocketReaderThreadH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <IdTCPConnection.hpp>
#include <IdTCPClient.hpp>
#include "AsteriskTools.h"

#include <FMX.Memo.hpp>
#include <FMX.StdCtrls.hpp>
#include "CMTWinRestClientForm.h"
#include "FMX.TMSMemo.hpp"

using namespace Asterisk::Tools;

// ---------------------------------------------------------------------------
class TSocketReaderThread : public TThread
	{
protected:
	void __fastcall Execute();

	AnsiString asLog;
	int BytesReceived;

private:
	Idtcpclient::TIdTCPClient * FTCPClient;

	Idtcpclient::TIdTCPClient * GetTCPClient();
	void SetTCPClient(Idtcpclient::TIdTCPClient * pTCPClient);

	TMemo *FMemo;

	TMemo *GetMemo();
	void SetMemo(TMemo *pMemo);
	void __fastcall Log(AnsiString asLogString);

	TLabel *FCounterLabel;
	TStringGrid *FQueGrid;
	TStringGrid *FExtGrid;
	TSwitch *FLogOutput;

	void __fastcall IncrementBytesReceived(int iBytes);
	void __fastcall WriteBytesReceived();
	void __fastcall HandleResponse(AnsiString RcvTxt);
	void __fastcall HandleEvent(AnsiString RcvTxt);
	void __fastcall HandleSubEvent(AnsiString RcvTxt);

	AsteriskTools AsteriskTool;

	bool __fastcall LoadMessage(NameValPair *Pairs);
	void __fastcall UpdateQue();
	void __fastcall UpdateAbandon();
	void __fastcall UpdateExtensionStatus();
	void __fastcall UpdateMemberStatus();
	void __fastcall UpdateAgentCalled();
	void __fastcall UpdateNewState();
	bool __fastcall GetColumnByName(TStringGrid *Grd, AnsiString ColumnName, int &ColumnIndex);

public:
	__fastcall TSocketReaderThread(bool CreateSuspended);
	__property Idtcpclient::TIdTCPClient * TCPClient =
		{read = GetTCPClient, write = SetTCPClient};
	__property TMemo *Memo =
		{read = GetMemo, write = SetMemo};
	void __fastcall WriteToMemo();
	__property TLabel *CounterLabel =
		{read = FCounterLabel, write = FCounterLabel};
	__property TStringGrid *QueGrid =
		{read = FQueGrid, write = FQueGrid};
	__property TStringGrid *ExtGrid =
		{read = FExtGrid, write = FExtGrid};
	__property TSwitch *LogOutput =
		{read = FLogOutput, write = FLogOutput};
	};
// ---------------------------------------------------------------------------
#endif
