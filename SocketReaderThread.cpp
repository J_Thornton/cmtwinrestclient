// ---------------------------------------------------------------------------

#include <System.hpp>
#include <System.SysUtils.hpp>
#include <System.DateUtils.hpp>
#include <System.JSON.hpp>

#pragma hdrstop

#include "SocketReaderThread.h"

#pragma package(smart_init)

using namespace Asterisk::Tools;
// ---------------------------------------------------------------------------

// Important: Methods and properties of objects in VCL can only be
// used in a method called using Synchronize, for example:
//
// Synchronize(&UpdateCaption);
//
// where UpdateCaption could look like:
//
// void __fastcall TSocketReaderThread::UpdateCaption()
// {
// Form1->Caption = "Updated in a thread";
// }
// ---------------------------------------------------------------------------
QueueCallerJoin QJoin;
QueueCallerAbandon QAbandon;
QueueMemberStatus QMemStatus;
AgentCalled AgtCalled;
AgentComplete AgtComplete;
AgentConnect AgtConnect;
ExtensionStatus ExtStatus;
Newstate NewState;

__fastcall TSocketReaderThread::TSocketReaderThread(bool CreateSuspended) : TThread(CreateSuspended)
	{
	FreeOnTerminate = true;
	FTCPClient = (Idtcpclient::TIdTCPClient*)NULL;
	BytesReceived = 0;
	FMemo = (TMemo*)NULL;
	FQueGrid = (TStringGrid*)NULL;
	}

void __fastcall TSocketReaderThread::Execute()
	{
	char CharRead;
	AnsiString Output = "";
	AnsiString strFix = "";
	NameThreadForDebugging(System::String(L"SocketReaderThread"));
	Log("TSocketReaderThread::Execute");
	bool bIsResponse = false;
	bool bIsEvent = false;
	bool bIsSubEvent = false;
	AnsiString JSONString = "";

	if (TCPClient)
		{
		if (TCPClient->Socket)
			{
			while (TCPClient->Socket->Connected() && Terminated == false && Output != NULL)
				{
				if (Output != NULL)
					{
					try
						{
						// ------------------
						// Read the next line
						// ------------------
						if (TCPClient->Connected())
							Output += TCPClient->IOHandler->ReadLn();
						else
							{
							Log("SocketReaderThread::Connected is false - Terminating");
							TCPClient->DisconnectNotifyPeer();
							Terminate();
							}
						}

					catch (EIdSocksError &EE)
						{
						Log(L"SocketReaderThread::Execute: IOHandler->ReadLn() EIdException:" + EE.Message);
						TCPClient->DisconnectNotifyPeer();
						Terminate();
						}
					catch (Exception &E)
						{
						Log(L"SocketReaderThread::Execute: IOHandler->ReadLn() Exception:" + E.Message);
						TCPClient->DisconnectNotifyPeer();
						Terminate();
						}

					try
						{
						// If the string is NOT blank
						UnicodeString Ustr = Output;

						// if (Output.Trim() != "" && AnsiString(Output.AnsiLastChar()) != "}")
						if (Output.Trim() != "" && UnicodeString(Ustr.LastChar()) != "}")
							{
							JSONString += Output;

							// Log(Output);
							IncrementBytesReceived(Output.Length());
							Output = "";
							}
						else
							{
							if (Output.Trim() != "")
								JSONString += Output;
							// -----------------------------------------------------------------------------------------
							// Acts differently in Windows than in other OS.
							// In Windows POS returns 0 if not found
							// In othe OS Pos returns -1 if not found
							// Quick fix to this is to put a character in front of the Output string and check for > 0
							// -----------------------------------------------------------------------------------------
							strFix = "X" + JSONString.Trim();
							bIsResponse = strFix.Pos(L"\"Response\":") > 0 ? true : false;
							bIsEvent = strFix.Pos(L"\"Event\":") > 0 ? true : false;
							bIsSubEvent = strFix.Pos(L"\"SubEvent\":") > 0 ? true : false;
							try
								{
								if (bIsResponse)
									HandleResponse(JSONString);
								if (bIsEvent)
									HandleEvent(JSONString);
								if (bIsSubEvent)
									HandleSubEvent(JSONString);
								}
							catch (Exception &E)
								{
								Log("Handle Exception:" + E.Message);
								}
							// Log("END:");
							JSONString = "";
							Output = "";
							}
						}
					catch (Exception &E)
						{
						Log("SocketReaderThread::Execute: Exception:" + E.Message);
						Terminate();
						}
					}
				}

			if (Output == NULL)
				{
				Log("TSocketReaderThread::Output is NULL");
				}

			if (TCPClient->Socket->Connected() == false)
				{
				Log("TSocketReaderThread::Socket is not connected");
				}

			if (Terminated)
				{
				Log("TSocketReaderThread::Thread is Terminated");
				}
			}
		else
			{
			Log("TSocketReaderThread::TCPClient is null");
			}
		}
	}

void __fastcall TSocketReaderThread::HandleResponse(AnsiString RcvTxt)
	{
	Log("----- TSocketReaderThread::HandleResponse -----");
	}

// RcvTxt = "Event: <EventName>"
void __fastcall TSocketReaderThread::HandleEvent(AnsiString RcvTxt)
	{
	AnsiString MyTmp = "";
	AnsiString EventName = "";
	NameValPair *Vals = new NameValPair();
	TStringDynArray MyStrings;
	AsteriskMgrEvent EventType = ameUnknown;
	TJSONObject *JObj = (TJSONObject*)NULL;
	String TimeStamp;
	AgentCalled *Agt = (AgentCalled*)NULL;

	// Parse the QueueParms from field in QueStatusMemTable
	// Entire Response
	try
		{
		JObj = (TJSONObject*) TJSONObject::ParseJSONValue(TEncoding::ASCII->GetBytes(RcvTxt), 0);
		if (JObj == NULL)
			return;
		EventName = JObj->GetValue("Event")->Value();
		TimeStamp = JObj->GetValue("TimeStamp")->Value();
		}
	catch (Exception &E)
		{
		Log("JSON Exception:" + E.Message);
		Log("RcvTxt:\n" + RcvTxt);
		return;
		}

	// There has to be at least one colon to parse
	if (RcvTxt.Pos(":") < 1)
		return;

	MyStrings = System::Strutils::SplitString(RcvTxt, ":");

	try
		{
		if (MyStrings.Length > 1)
			{
			EventType = AsteriskTool.EventFromName(EventName);
			switch (EventType)
				{
				// Dont want to see these. There are way too many to send to log
			case ameNewexten:
			case ameVarSet:
				break;
			default:
				Log("Event Name:(" + EventName + ") Type - " + IntToStr((int)EventType));
				break;
				}

			#pragma region Switch EventType
			switch (EventType)
				{
			case ameAgentCalled:
				Log("-- AgentCalled -- \r\n" + RcvTxt);
				// Agt = (AgentCalled *)  JObj->GetValue("");
				Agt = (AgentCalled*) JObj;

				AgtCalled.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				AgtCalled.Event = JObj->GetValue("Event")->Value();
				AgtCalled.Privilege = JObj->GetValue("Privilege")->Value();
				AgtCalled.Channel = JObj->GetValue("Channel")->Value();
				AgtCalled.ChannelState = JObj->GetValue("ChannelState")->Value();
				AgtCalled.ChannelStateDesc = JObj->GetValue("ChannelStateDesc")->Value();
				AgtCalled.CallerIDNum = JObj->GetValue("CallerIDNum")->Value();
				AgtCalled.CallerIDName = JObj->GetValue("CallerIDName")->Value();
				AgtCalled.ConnectedLineNum = JObj->GetValue("ConnectedLineNum")->Value();
				AgtCalled.ConnectedLineName = JObj->GetValue("ConnectedLineName")->Value();
				AgtCalled.Language = JObj->GetValue("Language")->Value();
				AgtCalled.AccountCode = JObj->GetValue("AccountCode")->Value();
				AgtCalled.Context = JObj->GetValue("Context")->Value();
				AgtCalled.Exten = JObj->GetValue("Exten")->Value();
				AgtCalled.Priority = JObj->GetValue("Priority")->Value();
				AgtCalled.Uniqueid = JObj->GetValue("Uniqueid")->Value();
				AgtCalled.Linkedid = JObj->GetValue("Linkedid")->Value();
				AgtCalled.DestChannel = JObj->GetValue("DestChannel")->Value();
				AgtCalled.DestChannelState = JObj->GetValue("DestChannelState")->Value();
				AgtCalled.DestChannelStateDesc = JObj->GetValue("DestChannelStateDesc")->Value();
				AgtCalled.DestCallerIDNum = JObj->GetValue("DestCallerIDNum")->Value();
				AgtCalled.DestCallerIDName = JObj->GetValue("DestCallerIDName")->Value();
				AgtCalled.DestConnectedLineNum = JObj->GetValue("DestConnectedLineNum")->Value();
				AgtCalled.DestConnectedLineName = JObj->GetValue("DestConnectedLineName")->Value();
				AgtCalled.DestLanguage = JObj->GetValue("DestLanguage")->Value();
				AgtCalled.DestAccountCode = JObj->GetValue("DestAccountCode")->Value();
				AgtCalled.DestContext = JObj->GetValue("DestContext")->Value();
				AgtCalled.DestExten = JObj->GetValue("DestExten")->Value();
				AgtCalled.DestPriority = JObj->GetValue("DestPriority")->Value();
				AgtCalled.DestUniqueid = JObj->GetValue("DestUniqueid")->Value();
				AgtCalled.DestLinkedid = JObj->GetValue("DestLinkedid")->Value();
				AgtCalled.Queue = JObj->GetValue("Queue")->Value();
				AgtCalled.Interface = JObj->GetValue("Interface")->Value();
				AgtCalled.MemberName = JObj->GetValue("MemberName")->Value();
				Synchronize(UpdateAgentCalled);
				break;
			case ameAgentComplete:
				break;
			case ameAgentConnect:
				break;
			case ameAgentDump:
				break;
			case ameAgentlogin:
				break;
			case ameAgentlogoff:
				break;
			case ameAgentRingNoAnswer:
				break;
			case ameAlarm:
				break;
			case ameAlarmClear:
				break;
			case ameBridge:
				break;
			case ameBridgeAction:
				break;
			case ameBridgeExec:
				break;
			case ameChanSpyStart:
				break;
			case ameChanSpyStop:
				break;
			case ameConfbridgeEnd:
				break;
			case ameConfbridgeJoin:
				break;
			case ameConfbridgeLeave:
				break;
			case ameConfbridgeStart:
				break;
			case ameConfbridgeTalking:
				break;
			case ameDAHDIChannel:
				break;
			case ameDial:
				break;
			case ameDNDState:
				break;
			case ameDTMF:
				break;
			case ameExtensionStatus:
				ExtStatus.Context = JObj->GetValue("Context")->Value();
				ExtStatus.Event = JObj->GetValue("Event")->Value();
				ExtStatus.Exten = JObj->GetValue("Exten")->Value();
				ExtStatus.Hint = JObj->GetValue("Hint")->Value();
				ExtStatus.Privilege = JObj->GetValue("Privilege")->Value();
				ExtStatus.Status = JObj->GetValue("Status")->Value();
				ExtStatus.StatusText = JObj->GetValue("StatusText")->Value();
				ExtStatus.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				Synchronize(UpdateExtensionStatus);
				break;
			case ameFullyBooted:
				break;
			case ameHangup:
				break;
			case ameHangupHandlerPop:
				break;
			case ameHangupHandlerPush:
				break;
			case ameHangupHandlerRun:
				break;
			case ameHangupRequest:
				break;
			case ameHold:
				Log("-- Hold --");
				break;
			case ameJoin:
				break;
			case ameLeave:
				break;
			case ameLocalBridge:
				break;
			case ameLogChannel:
				break;
			case ameMasquerade:
				break;
			case ameMeetmeEnd:
				break;
			case ameMeetmeJoin:
				break;
			case ameMeetmeLeave:
				break;
			case ameMeetmeMute:
				break;
			case ameMeetmeTalking:
				break;
			case ameMeetmeTalkRequest:
				break;
			case ameMessageWaiting:
				break;
			case ameModuleLoadReport:
				break;
			case ameNewAccountCode:
				break;
			case ameNewCallerid:
				break;
			case ameNewchannel:
				break;
			case ameNewexten:
				break;
			case ameNewPeerAccount:
				break;
			case ameNewstate:
				NewState.AccountCode = JObj->GetValue("AccountCode")->Value();
				NewState.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				NewState.Event = JObj->GetValue("Event")->Value();
				NewState.Privilege = JObj->GetValue("Privilege")->Value();
				NewState.Channel = JObj->GetValue("Channel")->Value();
				NewState.ChannelState = JObj->GetValue("ChannelState")->Value();
				NewState.ChannelStateDesc = JObj->GetValue("ChannelStateDesc")->Value();
				NewState.CallerIDNum = JObj->GetValue("CallerIDNum")->Value();
				NewState.CallerIDName = JObj->GetValue("CallerIDName")->Value();
				NewState.ConnectedLineNum = JObj->GetValue("ConnectedLineNum")->Value();
				NewState.ConnectedLineName = JObj->GetValue("ConnectedLineName")->Value();
				NewState.Language = JObj->GetValue("Language")->Value();
				NewState.AccountCode = JObj->GetValue("AccountCode")->Value();
				NewState.Context = JObj->GetValue("Context")->Value();
				NewState.Exten = JObj->GetValue("Exten")->Value();
				NewState.Priority = JObj->GetValue("Priority")->Value();
				NewState.Uniqueid = JObj->GetValue("Uniqueid")->Value();
				NewState.Linkedid = JObj->GetValue("Linkedid")->Value();
				Synchronize(UpdateNewState);
				break;
			case ameOriginateResponse:
				break;
			case ameParkedCall:
				break;
			case ameParkedCallGiveUp:
				break;
			case ameParkedCallTimeOut:
				break;
			case amePickup:
				break;
			case amePresenceStatus:
				break;
			case ameDeviceStateChange:
				Log("-- DeviceStateChange --(" + EventName + ")");
				break;
			case ameQueueCallerJoin:
				Log("-- QueueCallerJoin -- Count = " + JObj->GetValue("Count")->Value());
				QJoin.AccountCode = JObj->GetValue("AccountCode")->Value();
				QJoin.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				QJoin.Event = JObj->GetValue("Event")->Value();
				QJoin.Privilege = JObj->GetValue("Privilege")->Value();
				QJoin.Channel = JObj->GetValue("Channel")->Value();
				QJoin.ChannelState = JObj->GetValue("ChannelState")->Value();
				QJoin.ChannelStateDesc = JObj->GetValue("ChannelStateDesc")->Value();
				QJoin.CallerIDNum = JObj->GetValue("CallerIDNum")->Value();
				QJoin.CallerIDName = JObj->GetValue("CallerIDName")->Value();
				QJoin.ConnectedLineNum = JObj->GetValue("ConnectedLineNum")->Value();
				QJoin.ConnectedLineName = JObj->GetValue("ConnectedLineName")->Value();
				QJoin.Language = JObj->GetValue("Language")->Value();
				QJoin.AccountCode = JObj->GetValue("AccountCode")->Value();
				QJoin.Context = JObj->GetValue("Context")->Value();
				QJoin.Exten = JObj->GetValue("Exten")->Value();
				QJoin.Priority = JObj->GetValue("Priority")->Value();
				QJoin.Uniqueid = JObj->GetValue("Uniqueid")->Value();
				QJoin.Linkedid = JObj->GetValue("Linkedid")->Value();
				QJoin.Queue = JObj->GetValue("Queue")->Value();
				QJoin.Position = JObj->GetValue("Position")->Value();
				QJoin.Count = JObj->GetValue("Count")->Value();
				Synchronize(UpdateQue);
				break;
			case ameQueueCallerLeave:
				Log("-- QueueCallerLeave -- Count = " + JObj->GetValue("Count")->Value());
				QJoin.AccountCode = JObj->GetValue("AccountCode")->Value();
				QJoin.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				QJoin.Event = JObj->GetValue("Event")->Value();
				QJoin.Privilege = JObj->GetValue("Privilege")->Value();
				QJoin.Channel = JObj->GetValue("Channel")->Value();
				QJoin.ChannelState = JObj->GetValue("ChannelState")->Value();
				QJoin.ChannelStateDesc = JObj->GetValue("ChannelStateDesc")->Value();
				QJoin.CallerIDNum = JObj->GetValue("CallerIDNum")->Value();
				QJoin.CallerIDName = JObj->GetValue("CallerIDName")->Value();
				QJoin.ConnectedLineNum = JObj->GetValue("ConnectedLineNum")->Value();
				QJoin.ConnectedLineName = JObj->GetValue("ConnectedLineName")->Value();
				QJoin.Language = JObj->GetValue("Language")->Value();
				QJoin.AccountCode = JObj->GetValue("AccountCode")->Value();
				QJoin.Context = JObj->GetValue("Context")->Value();
				QJoin.Exten = JObj->GetValue("Exten")->Value();
				QJoin.Priority = JObj->GetValue("Priority")->Value();
				QJoin.Uniqueid = JObj->GetValue("Uniqueid")->Value();
				QJoin.Linkedid = JObj->GetValue("Linkedid")->Value();
				QJoin.Queue = JObj->GetValue("Queue")->Value();
				QJoin.Position = JObj->GetValue("Position")->Value();
				QJoin.Count = JObj->GetValue("Count")->Value();
				Synchronize(UpdateQue);
				break;

			case ameQueueCallerAbandon:
				Log("-- QueueCallerAbandon -- Hold Time = " + JObj->GetValue("HoldTime")->Value());
				QAbandon.AccountCode = JObj->GetValue("AccountCode")->Value();
				QAbandon.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				QAbandon.Event = JObj->GetValue("Event")->Value();
				QAbandon.Privilege = JObj->GetValue("Privilege")->Value();
				QAbandon.Channel = JObj->GetValue("Channel")->Value();
				QAbandon.ChannelState = JObj->GetValue("ChannelState")->Value();
				QAbandon.ChannelStateDesc = JObj->GetValue("ChannelStateDesc")->Value();
				QAbandon.CallerIDNum = JObj->GetValue("CallerIDNum")->Value();
				QAbandon.CallerIDName = JObj->GetValue("CallerIDName")->Value();
				QAbandon.ConnectedLineNum = JObj->GetValue("ConnectedLineNum")->Value();
				QAbandon.ConnectedLineName = JObj->GetValue("ConnectedLineName")->Value();
				QAbandon.Language = JObj->GetValue("Language")->Value();
				QAbandon.AccountCode = JObj->GetValue("AccountCode")->Value();
				QAbandon.Context = JObj->GetValue("Context")->Value();
				QAbandon.Exten = JObj->GetValue("Exten")->Value();
				QAbandon.Priority = JObj->GetValue("Priority")->Value();
				QAbandon.Uniqueid = JObj->GetValue("Uniqueid")->Value();
				QAbandon.Linkedid = JObj->GetValue("Linkedid")->Value();
				QAbandon.Queue = JObj->GetValue("Queue")->Value();
				QAbandon.Position = JObj->GetValue("Position")->Value();
				QAbandon.OriginalPosition = JObj->GetValue("OriginalPosition")->Value();
				QAbandon.HoldTime = JObj->GetValue("HoldTime")->Value();
				Synchronize(UpdateAbandon);

				break;
			case ameQueueMemberAdded:
				break;
			case ameQueueMemberPaused:
				break;
			case ameQueueMemberPenalty:
				break;
			case ameQueueMemberRemoved:
				break;
			case ameQueueMemberRinginuse:
				break;
			case ameQueueMemberStatus:
				Log("-- QueueMemberStatus --\r\n" + RcvTxt);
				QMemStatus.TimeStamp = JObj->GetValue("TimeStamp")->Value();
				QMemStatus.Event = JObj->GetValue("Event")->Value();
				QMemStatus.Privilege = JObj->GetValue("Privilege")->Value();
				QMemStatus.Queue = JObj->GetValue("Queue")->Value();
				QMemStatus.MemberName = JObj->GetValue("MemberName")->Value();
				QMemStatus.Interface = JObj->GetValue("Interface")->Value();
				QMemStatus.StateInterface = JObj->GetValue("StateInterface")->Value();
				QMemStatus.Membership = JObj->GetValue("Membership")->Value();
				QMemStatus.Penalty = JObj->GetValue("Penalty")->Value();
				QMemStatus.CallsTaken = JObj->GetValue("CallsTaken")->Value();
				QMemStatus.LastCall = JObj->GetValue("LastCall")->Value();
				QMemStatus.InCall = JObj->GetValue("InCall")->Value();
				QMemStatus.Status = JObj->GetValue("Status")->Value();
				QMemStatus.Paused = JObj->GetValue("Paused")->Value();
				QMemStatus.PausedReason = JObj->GetValue("PausedReason")->Value();
				QMemStatus.Ringinuse = JObj->GetValue("Ringinuse")->Value();
				Synchronize(UpdateMemberStatus);
				break;
			case ameRename:
				break;
			case ameShutdown:
				break;
			case ameSoftHangupRequest:
				break;
			case ameSpanAlarm:
				break;
			case ameSpanAlarmClear:
				break;
			case ameUnParkedCall:
				break;
			case ameUserEvent:
				break;
			case ameVarSet:
				break;
			case amePeerStatus:
				Log("-- PeerStatus -- " + EventName);
				break;
			case ameUnknown:
				Log("ameUnknown:(" + EventName + ")");
				break;
			case ameRegistry:
				break;
			default:
				break;
				}
			#pragma end_region

			}
		}
	catch (Exception &E)
		{
		Log("TSocketReaderThread::HandleEvent - Exception:" + E.Message);
		}
	}

void __fastcall TSocketReaderThread::UpdateAgentCalled()
	{
	int iRow, iCol, iStatus;
	//
	// ExtGrid is NULL return
	//
	if (ExtGrid == NULL)
		return;
	//
	// If we cant find the column named CallerID return
	//
	if (GetColumnByName(ExtGrid, "CallerID", iCol) == false)
		return;
	//
	// Find the Row that we need to update based on the Extension
	//
	for (iRow = 0; iRow < ExtGrid->RowCount; iRow++)
		{
		if (ExtGrid->Cells[iCol][iRow].Trim() == AgtCalled.MemberName.Trim())
			break;
		}
	//
	// Did not find matching row with Extension = ExtStatus.Exten return
	//
	if (iRow == ExtGrid->RowCount)
		return;
	//
	// Get the columun index for Call Data
	//
	if (GetColumnByName(ExtGrid, "Call Data", iCol))
		ExtGrid->Cells[iCol][iRow] = AgtCalled.CallerIDName + ":" + AgtCalled.CallerIDNum;

	ExtGrid->Repaint();
	Application->ProcessMessages();
	}

void __fastcall TSocketReaderThread::UpdateNewState()
	{
	int iRow, iCol, iStatus;
	FMemo->Lines->Add("UpdateNewState()");
	//
	// ExtGrid is NULL return
	//
	if (ExtGrid == NULL)
		return;
	//
	// If we cant find the column named Extension return
	//
	if (GetColumnByName(ExtGrid, "Extension", iCol) == false)
		return;
	//
	// Find the Row that we need to update based on the Extension
	//
	FMemo->Lines->Add("UpdateNewState()::CallerIDNum:" + NewState.CallerIDNum.Trim());
	for (iRow = 0; iRow < ExtGrid->RowCount; iRow++)
		{
		if (ExtGrid->Cells[iCol][iRow].Trim() == NewState.CallerIDNum.Trim())
			break;
		}
	//
	// Did not find matching row with Extension = ExtStatus.Exten return
	//
	if (iRow == ExtGrid->RowCount)
		return;
	//
	// Get the columun index for Call Data
	//
	if (GetColumnByName(ExtGrid, "Call Data", iCol))
		{
		FMemo->Lines->Add("UpdateNewState()::ChannelState:" + NewState.ChannelState.Trim());
		// Based on the ChannelState set the CallData
		switch (StrToInt(NewState.ChannelState))
			{
			// Ring
		case 4:
			ExtGrid->Cells[iCol][iRow] = "Calling:" + NewState.Exten;
			break;
			// Ringing
		case 5:
			ExtGrid->Cells[iCol][iRow] = "Call from:" + NewState.ConnectedLineName + ":" + NewState.ConnectedLineNum;
			break;
			}

		}

	ExtGrid->Repaint();
	Application->ProcessMessages();
	}

void __fastcall TSocketReaderThread::UpdateMemberStatus()
	{
	int iRow, iCol;
	if (GetColumnByName(QueGrid, "Queue", iCol) == false)
		return;

	// Find the Row that we need to update
	for (iRow = 0; iRow < QueGrid->RowCount; iRow++)
		{
		if (QueGrid->Cells[iCol][iRow].Trim() == QMemStatus.Queue.Trim())
			break;
		}

	// Did not find matching row with Queue = QMemStatus.Queue
	if (iRow == QueGrid->RowCount)
		return;

	if (GetColumnByName(QueGrid, "Calls", iCol) == false)
		return;

	// we are not doing this because UpdateQue keeps track
	// Set the Cell to the QMemStatus.Calls
	// QueGrid->Cells[iCol][iRow] = QMemStatus.Ringinuse;
	// QueGrid->Repaint();
	// Application->ProcessMessages();
	}

void __fastcall TSocketReaderThread::UpdateExtensionStatus()
	{
	int iRow, iCol, iStatus, iDataCol;
	//
	// ExtGrid is NULL return
	//
	if (ExtGrid == NULL)
		return;
	//
	// If we cant find the column named Extension return
	//
	if (GetColumnByName(ExtGrid, "Extension", iCol) == false)
		return;
	//
	// Find the Row that we need to update based on the Extension
	//
	for (iRow = 0; iRow < ExtGrid->RowCount; iRow++)
		{
		if (ExtGrid->Cells[iCol][iRow].Trim() == ExtStatus.Exten.Trim())
			break;
		}
	//
	// Did not find matching row with Extension = ExtStatus.Exten return
	//
	if (iRow == ExtGrid->RowCount)
		return;
	//
	// Get the columun index for State
	//
	if (GetColumnByName(ExtGrid, "State", iCol))
		ExtGrid->Cells[iCol][iRow] = ExtStatus.Status + " - " + ExtStatus.StatusText;
	//
	// Get the Column index for Status
	//
	if (GetColumnByName(ExtGrid, "Status", iCol) == false)
		return;

	if (GetColumnByName(ExtGrid, "Call Data", iDataCol) == false)
		return;

	// TODO: Extension States are bit shifted so they can have more than one state

	iStatus = StrToInt(ExtStatus.Status);

	switch (iStatus)
		{
		// Extension Hint removed
	case -1:
		ExtGrid->Cells[iCol][iRow] = 6;
		break;
		// Extension Removed
	case -2:
		ExtGrid->Cells[iCol][iRow] = 7;
		ExtGrid->Cells[iDataCol][iRow] = "";
		break;
		// Idle
	case 0:
		ExtGrid->Cells[iCol][iRow] = 0;
		ExtGrid->Cells[iDataCol][iRow] = "";
		break;
		// InUse
	case 1:
		ExtGrid->Cells[iCol][iRow] = 1;
		break;
		// Busy
	case 2:
		ExtGrid->Cells[iCol][iRow] = 2;
		break;
		// Unavailable
	case 4:
		ExtGrid->Cells[iCol][iRow] = 3;
		ExtGrid->Cells[iDataCol][iRow] = "";
		break;
		// Ringing
	case 8:
		ExtGrid->Cells[iCol][iRow] = 4;
		break;
		// On Hold
	case 16:
		ExtGrid->Cells[iCol][iRow] = 5;
		break;

		// Otherwise check the bit state
	default:

		break;
		}

	ExtGrid->Repaint();
	Application->ProcessMessages();

	}

void __fastcall TSocketReaderThread::UpdateAbandon()
	{
	int iRow, iCol;
	if (GetColumnByName(QueGrid, "Queue", iCol) == false)
		return;

	// Find the Row that we need to update
	for (iRow = 0; iRow < QueGrid->RowCount; iRow++)
		{
		if (QueGrid->Cells[iCol][iRow].Trim() == QAbandon.Queue.Trim())
			break;
		}

	// Did not find matching row with Queue = QJoin.Queue
	if (iRow == QueGrid->RowCount)
		return;

	if (GetColumnByName(QueGrid, "Abandoned", iCol) == false)
		return;

	// Add 1 to Abandon
	// Set the Cell to the QJoin.Count
	if (QueGrid->Cells[iCol][iRow] == "")
		QueGrid->Cells[iCol][iRow] = "1";
	else
		QueGrid->Cells[iCol][iRow] = IntToStr(StrToInt(QueGrid->Cells[iCol][iRow]) + 1);

	// Update Hold Time
	if (GetColumnByName(QueGrid, "HoldTime", iCol))
		{
		QueGrid->Cells[iCol][iRow] = QAbandon.HoldTime;
		}

	QueGrid->Repaint();
	Application->ProcessMessages();
	}

void __fastcall TSocketReaderThread::UpdateQue()
	{
	int iRow, iCol;

	if (GetColumnByName(QueGrid, "Queue", iCol) == false)
		return;

	// Find the Row that we need to update
	for (iRow = 0; iRow < QueGrid->RowCount; iRow++)
		{
		if (QueGrid->Cells[iCol][iRow].Trim() == QJoin.Queue.Trim())
			break;
		}

	// Did not find matching row with Queue = QJoin.Queue
	if (iRow == QueGrid->RowCount)
		return;

	// We are now on the correct Row for the Queue. Now we need to find the column with a header of Calls
	// first find the column named Queue

	if (GetColumnByName(QueGrid, "Calls", iCol) == false)
		return;

	// Set the Cell to the QJoin.Count
	QueGrid->Cells[iCol][iRow] = QJoin.Count;
	QueGrid->Repaint();
	Application->ProcessMessages();
	}

bool __fastcall TSocketReaderThread::GetColumnByName(TStringGrid *Grd, AnsiString ColumnName, int &ColumnIndex)
	{
	int OriginalColumnIndex = ColumnIndex;
	if (Grd == NULL)
		return (false);

	for (ColumnIndex = 0; ColumnIndex < Grd->ColumnCount; ColumnIndex++)
		{
		if (Grd->ColumnByIndex(ColumnIndex)->Header.Trim() == ColumnName)
			return (true);
		}
	ColumnIndex = OriginalColumnIndex;
	return (false);
	}

bool __fastcall TSocketReaderThread::LoadMessage(NameValPair * Pairs)
	{
	bool bKeepReading = true;
	AnsiString MyTmp = "";
	TStringDynArray MyStrings;
	Pairs->Clear();

	while (bKeepReading)
		{
		try
			{
			MyTmp = TCPClient->IOHandler->ReadLn();
			if (MyTmp != "")
				{
				MyStrings = SplitString(MyTmp, ":");
				switch (MyStrings.Length)
					{
				case 0:
					Pairs->Add(MyTmp, "");
					break;
				case 1:
					Pairs->Add(MyStrings[0], "");
					break;
				case 2:
					Pairs->Add(MyStrings[0], MyStrings[1]);
					break;
				default:
					break;
					}

				}
			else
				{
				// End of read
				bKeepReading = false;
				}
			}
		catch (Exception &E)
			{
			Log("TSocketReaderThread::LoadMessage Exception:" + E.Message);
			TCPClient->Disconnect();
			}
		}
	// ---------------------------
	// Success if the count is > 0
	// ---------------------------
	return (Pairs->Count > 0 ? true : false);
	}

void __fastcall TSocketReaderThread::HandleSubEvent(AnsiString RcvTxt)
	{

	}

void __fastcall TSocketReaderThread::Log(AnsiString asOut)
	{
	if (LogOutput != NULL)
		{
		if (LogOutput->IsChecked == false)
			return;
		}
	UnicodeString USNow = "";
	unsigned short sHH, sMM, sSS, sMS;
	TDateTime DTNow = Now();
	DTNow.DecodeTime(&sHH, &sMM, &sSS, &sMS);

	USNow = DTNow.FormatString(L"hh:NN:ss") + USNow.sprintf(L".%.3d", sMS);

	asLog = USNow + ":" + asOut;
	Synchronize(WriteToMemo);
	}

Idtcpclient::TIdTCPClient * TSocketReaderThread::GetTCPClient()
	{
	return (FTCPClient);
	}

void TSocketReaderThread::SetTCPClient(Idtcpclient::TIdTCPClient * pTCPClient)
	{
	FTCPClient = pTCPClient;
	}

void __fastcall TSocketReaderThread::IncrementBytesReceived(int NumBytes)
	{
	BytesReceived = NumBytes;
	Synchronize(WriteBytesReceived);
	}

void __fastcall TSocketReaderThread::WriteBytesReceived()
	{
	int TotalBytes = 0;
	try
		{
		TotalBytes = StrToInt(CounterLabel->Text.Trim()) + BytesReceived;
		}
	catch (Exception &E)
		{
		TotalBytes = BytesReceived;
		}

	BytesReceived = 0;
	CounterLabel->Text = IntToStr(TotalBytes);
	}

void __fastcall TSocketReaderThread::WriteToMemo()
	{
	if (Memo->Lines->Count > 5000)
		Memo->Lines->Clear();
	Memo->Lines->Add(asLog);
	Memo->Repaint();
	Application->ProcessMessages();
	}

void TSocketReaderThread::SetMemo(TMemo * pMemo)
	{
	FMemo = pMemo;
	}

TMemo *TSocketReaderThread::GetMemo()
	{
	return (FMemo);
	}
