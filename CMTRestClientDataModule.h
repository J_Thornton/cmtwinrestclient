// ---------------------------------------------------------------------------

#ifndef CMTRestClientDataModuleH
#define CMTRestClientDataModuleH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <IPPeerClient.hpp>
#include <REST.Client.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageBin.hpp>
#include <REST.Response.Adapter.hpp>
#include <FireDAC.DApt.hpp>
#include <REST.Types.hpp>

// ---------------------------------------------------------------------------
class TdmRestClient : public TDataModule
	{
__published: // IDE-managed Components
	TRESTClient * AsteriskClient;
	TRESTRequest * P2DRequest;
	TRESTResponse * P2DResponse;
	TRESTResponseDataSetAdapter *ExtDataAdapter;
	TBindSourceDB * ExtBindSource;
	TFDMemTable * ExtMemTable;
	TRESTRequest * ExtRequest;
	TRESTResponse * ExtResponse;
	TRESTRequest * LoginRequest;
	TRESTResponse * LoginResponse;
	TRESTResponseDataSetAdapter *LoginDataAdapter;
	TFDMemTable * LoginMemTable;
	TBindSourceDB * LoginBindSource;
	TRESTRequest *QueStatusRequest;
	TRESTResponse *QueStatusResponse;
	TRESTResponseDataSetAdapter *QueStatusDataAdapter;
	TFDMemTable *QueStatusMemTable;
	TBindSourceDB *QueStatusBindSource;
	TRESTResponseDataSetAdapter *QueParmsDataAdapter;
	TFDMemTable *QueParmsMemTable;
	TWideStringField *WideStringField1;
	TWideStringField *WideStringField2;
	TWideStringField *WideStringField3;
	TWideStringField *WideStringField4;
	TWideStringField *WideStringField5;
	TWideStringField *WideStringField6;
	TBindSourceDB *QueParmsBindSource;
	TWideStringField *QueStatusMemTableTimeStamp;
	TWideStringField *QueStatusMemTableCommand;
	TWideStringField *QueStatusMemTableActionID;
	TWideStringField *QueStatusMemTableResult;
	TWideStringField *QueStatusMemTableQueueParams;
	TWideStringField *QueStatusMemTableResultCode;
	TWideStringField *QueStatusMemTableErrorMsg;
	TRESTRequest *QueResetStatsRequest;
	TRESTResponse *QueResetStatsResponse;
	TRESTRequest *ExtStatusRequest;
	TRESTResponse *ExtStatusResponse;
	TRESTResponseDataSetAdapter *ExtStatusAdapter;
	TFDMemTable *ExtStatusMemTable;
	TBindSourceDB *ExtStatusBindSource;
	TRESTClient *IVRClient;
	TRESTRequest *IVRGetCustRequest;
	TRESTResponse *IVRGetCustResponse;
	TRESTRequest *IVRCancelRequest;
	TRESTResponse *IVRCancelResponse;
	TRESTRequest *IVRBookRequest;
	TRESTResponse *IVRBookResponse;
	TRESTRequest *GetCalloutRequest;
	TRESTResponse *GetCalloutResponse;
	TRESTClient *GoogleGeocodeClient;
	TRESTRequest *GoogleGeocodeRequest;
	TRESTResponse *GoogleGeocodeResponse;
	TRESTResponseDataSetAdapter *GoogleGeocodeAdapter;
	TFDMemTable *GoogleGeocodeMemTable;
	TWideStringField *GoogleGeocodeMemTableresults;
	TWideStringField *GoogleGeocodeMemTablestatus;

private: // User declarations
public:  // User declarations
	__fastcall TdmRestClient(TComponent* Owner);
	};

// ---------------------------------------------------------------------------
extern PACKAGE TdmRestClient *dmRestClient;
// ---------------------------------------------------------------------------
#endif
