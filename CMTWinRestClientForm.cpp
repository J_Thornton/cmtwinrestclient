// ---------------------------------------------------------------------------

#include <fmx.h>
#include <System.UITypes.hpp>
#pragma hdrstop

#include "CMTWinRestClientForm.h"
#include "CMTRestClientDataModule.h"
#include "SocketReaderThread.h"
#include <System.JSON.hpp>
#include <DePickler.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <cstdio>
#include <fstream>
#include <cassert>
#include <functional>

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "FMX.TMSBaseControl"
#pragma link "FMX.TMSCustomGrid"
#pragma link "FMX.TMSGrid"
#pragma link "FMX.TMSGridCell"
#pragma link "FMX.TMSGridData"
#pragma link "FMX.TMSGridOptions"
#pragma link "FMX.TMSCustomTreeView"
#pragma link "FMX.TMSTreeView"
#pragma link "FMX.TMSTreeViewBase"
#pragma link "FMX.TMSTreeViewData"
#pragma link "FMX.TMSLed"
#pragma link "FMX.TMSCheckGroupPicker"
#pragma link "FMX.TMSCustomPicker"
#ifndef TARGET_OS_MAC
#pragma link "sgcWebSocket"
#pragma link "sgcWebSocket_Classes"
#pragma link "sgcWebSocket_Classes_Indy"
#pragma link "sgcWebSocket_Client"
#endif
#pragma link "FMX.TMSCustomGrid"
#pragma resource "*.fmx"

TfrmRestClient *frmRestClient;
extern TdmRestClient *dmRestClient;
TSocketReaderThread *Thrd;
UnicodeString UStr = "";
AsteriskTools *Tools;
DePickler *LogDePickler;

// ---------------------------------------------------------------------------
__fastcall TfrmRestClient::TfrmRestClient(TComponent* Owner) : TForm(Owner)
	{
	aniBusy->Visible = false;
	aniExtensions->Visible = false;
	grdExtensions->Enabled = true; // For some reason in Windows this is not enabled
	IsLoggedIn = false;
	Token = "";
	JResponseObj = (TJSONObject*)NULL;
	Thrd = (TSocketReaderThread*)NULL;
	Tools = new AsteriskTools();
	LogDePickler = new DePickler();

	// Bug: The MultiView is messed up. The first time it is expanded, it overwrites the
	// Detail form. The second time it is fine.
	MultiView1->Enabled = false;
	MultiView1->Visible = false;
	MultiView1->ShowMaster();
	MultiView1->HideMaster();
	MultiView1->UpdateEffects();
	MultiView1->Enabled = true;
	MultiView1->Visible = true;
	// Set the column titles for the IVR Grid
	grdEIVR->Cells[0][0] = "CallerID";
	grdEIVR->Cells[1][0] = "Description";
	grdEIVR->Cells[2][0] = "Booking";
	grdEIVR->Cells[3][0] = "TripStatus";
	grdEIVR->Cells[4][0] = "Cancel";
	// these two need to be called or the GetObjects will return NULL (TTMSFMXGrid)
	// this happens if the grid is not visible because its on a different tab
	grdEIVR->NeedStyleLookup();
	grdEIVR->ApplyStyleLookup();
	// Turn this off or you cant change the time in BookOrder
	teTripTime->UseNowTime = false;
	lblDistanceETA->Text = "";
	lblTripIDStatus->Text = "";
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::FormCreate(TObject *Sender)
	{
	// This defines the default active tab at runtime
	TabControl1->ActiveTab = tabLogin;
	TabControl1->TabPosition = TTabPosition::None; // Get rid of Tabs

	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::FormShow(TObject *Sender)
	{
	dmRestClient->ExtMemTable->Active = false;
	grdExtensions->Repaint();
	grdExtensions->ShowScrollBars = true;
	lblToken->Text = "";
	lblLoginResponse->Text = "";
	lblLoginErrorMsg->Text = "";
	lblLoginResult->Text = "";
	lblLoginTime->Text = "";
	lblTimeStamp->Text = "";
	lblTenantName->Text = "";
	lblLoginStatus->Text = "";
	lblSettingsStatus->Text = "";
	}

void __fastcall TfrmRestClient::Button2Click(TObject *Sender)
	{
	try
		{
		dmRestClient->P2DRequest->Params->ParameterByName("Token")->Value = lblToken->Text;
		dmRestClient->P2DRequest->Execute();
		}
	catch (Exception &E)
		{
		Log("P2DRequest->Execute() Exception:" + E.Message);
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::UpdateClearedText(TClearEditButton *ClearButton, TEdit *EditField)
	{
	EditField->SetFocus();
	EditField->Text = "";
	ClearButton->SetFocus();
	}

void __fastcall TfrmRestClient::clrCallerIDClick(TObject *Sender)
	{
	UpdateClearedText((TClearEditButton *)Sender, edtCallerID);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::clrDriverPhoneClick(TObject *Sender)
	{
	UpdateClearedText((TClearEditButton *)Sender, edtDriverPhone);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::clrPassengerPhoneClick(TObject *Sender)
	{
	UpdateClearedText((TClearEditButton *)Sender, edtPassengerPhone);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::clrBaseURLClick(TObject *Sender)
	{
	edtBaseURL->SetFocus();
	edtBaseURL->Text = "http://";
	clrBaseURL->SetFocus();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::btnRetrieveClick(TObject *Sender)
	{
	int iTmp = 0;
	try
		{
		// Clear the Grid
		dmRestClient->ExtMemTable->Active = false;
		// Get the new Grid values
		Log("::btnRetrieve - Request: Token:" + lblToken->Text);
		dmRestClient->AsteriskClient->BaseURL = edtBaseURL->Text;
		dmRestClient->ExtRequest->Params->ParameterByName("Token")->Value = lblToken->Text;
		try
			{
			dmRestClient->ExtRequest->Execute();
			}
		catch (Exception &E)
			{
			Log("dmRestClient->ExtRequest->Execute() Exception:" + E.Message);
			return;
			}

		Log("::btnRetrieve - Response:" + dmRestClient->ExtResponse->Content);
		if (dmRestClient->ExtResponse->Status.Success())
			{
			// Parse the QueueParms from field in QueStatusMemTable
			// Entire Response
			JResponseObj = (TJSONObject*) TJSONObject::ParseJSONValue
				 (TEncoding::ASCII->GetBytes(dmRestClient->ExtResponse->Content), 0);
			// Should be Array of Extensions
			TJSONArray *JQParamArray = (TJSONArray*)JResponseObj->Get("Extensions")->JsonValue;
			grdExtensions->RowCount = JQParamArray->Count;
			for (int ii = 0; ii < JQParamArray->Count; ii++)
				{
				TJSONObject *QParam = (TJSONObject*) JQParamArray->Items[ii];
				// This is the index of the ImageList attached to the grdExtensions
				// The Column is defined as a GlyphColumn
				grdExtensions->Cells[0][ii] = 0;
				grdExtensions->Cells[2][ii] = QParam->GetValue("extension")->Value();
				// Not all Extensions have a dial and callerid so we have to make sure that they
				// get checked
				try
					{
					grdExtensions->Cells[3][ii] = QParam->GetValue("dial")->Value();
					}
				catch (...)
					{
					grdExtensions->Cells[3][ii] = " ";
					}
				try
					{
					grdExtensions->Cells[4][ii] = QParam->GetValue("callerid")->Value();
					if ((iTmp = grdExtensions->Cells[4][ii].Pos(L"<")) > 0)
						grdExtensions->Cells[4][ii] = grdExtensions->Cells[4][ii].SubString(0, iTmp - 1);
					}
				catch (...)
					{
					grdExtensions->Cells[4][ii] = " ";
					}
				grdExtensions->Cells[5][ii] = QParam->GetValue("OutboundCID")->Value();
				grdExtensions->Cells[6][ii] = "";
				}
			grdExtensions->ResetFocus();
			SizeGridColumns(grdExtensions);
			grdExtensions->SetFocus();
			tabExtensions->SetFocus();
			}
		}
	catch (Exception &E)
		{
		Log("::btnRetrieve - Exception:" + E.Message);
		Log("::btnRetrieve - Response:" + dmRestClient->ExtResponse->Content);

		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::clrCategoryClick(TObject *Sender)
	{
	UpdateClearedText((TClearEditButton *)Sender, edtExtCategory);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::grdExtensionsDrawColumnCell(TObject *Sender, TCanvas * const Canvas,
	 TColumn * const Column, const TRectF &Bounds, const int Row, const TValue &Value, const TGridDrawStates State)
	{
	float ColWidth;
	TRectF R = grdExtensions->AbsoluteRect;

	if ((ColWidth = Canvas->TextWidth(grdExtensions->Cells[Column->Index][Row])) > Column->Width)
		{
		Column->Width = ColWidth + 8;
		grdExtensions->InvalidateRect(Bounds);
		}

	}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TabControl1Change(TObject *Sender)
	{
	if (TabControl1->ActiveTab == tabExtensions)
		{
		if (grdExtensions->CanFocus)
			grdExtensions->ScrollToTop();
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::lbP2DClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabP2D;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbLogClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabLog;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbExtensionsClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabExtensions;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbSettingsClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabSettings;
	lbSettings->TagObject = tabSettings;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbLoginClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabLogin;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnSubmitLoginClick(TObject *Sender)
	{
	UnicodeString USNow;
	// find the center of the screen
	aniBusy->Position->X = (int)(this->Width - (int)aniBusy->Width) / 2;
	aniBusy->Position->Y = (int)(this->Height - (int)aniBusy->Height) / 2;
	aniBusy->Enabled = true;
	aniBusy->Visible = true;
	Application->ProcessMessages();
	try
		{
		// Clear the Extensions Grid
		dmRestClient->ExtMemTable->Active = false;
		// Clear Login Output
		lblToken->Text = "";
		lblLoginResponse->Text = "";
		lblLoginErrorMsg->Text = "";
		lblLoginResult->Text = "";
		lblLoginTime->Text = "";
		lblTimeStamp->Text = "";
		lblTenantName->Text = "";
		// Execute the Login REST request
		Application->ProcessMessages();
		lblLoginStatus->Text = "Logging In: " + dmRestClient->LoginRequest->Client->BaseURL;
		try
			{
			dmRestClient->LoginRequest->Execute();
			}
		catch (Exception &E)
			{
			Log("dmRestClient->LoginRequest->Execute():Exception:" + E.Message);
			Log("Content:" + dmRestClient->LoginResponse->Content);
			aniBusy->Enabled = false;
			aniBusy->Visible = false;
			TabControl1->Repaint();
			Application->ProcessMessages();
			return;
			}

		Application->ProcessMessages();
		// Check if request was successful as far as going to the REST Service
		if (dmRestClient->LoginResponse->Status.Success())
			{
			// Login Success
			if (dmRestClient->LoginMemTable->Fields->FieldByName("Result")->AsString == "Success")
				{
				// Set the LoginTime
				unsigned short sHH, sMM, sSS, sMS;
				TDateTime DTNow = Now();
				DTNow.DecodeTime(&sHH, &sMM, &sSS, &sMS);
				lblLoginTime->Text = DTNow.FormatString("YYYY-mm-dd h:nn:ss") + USNow.sprintf(L".%.3d", sMS);
				// Set the Token
				Token = dmRestClient->LoginMemTable->Fields->FieldByName("Token")->AsString;
				IsLoggedIn = true;
				lblLoginStatus->Text = "Getting extension list";
				// Get the Extension list
				btnRetrieveClick(Sender);
				Application->ProcessMessages();
				// Get Each Extensions Current Status
				lblLoginStatus->Text = "Getting extension status for all extensions";
				GetExtensionStates();
				Application->ProcessMessages();
				// Connect to JSON
				lblLoginStatus->Text = "Connecting to JSON host";
				btnJSONConnectClick(Sender);
				Application->ProcessMessages();
				// Get Queue Status
				lblLoginStatus->Text = "Getting queue status";
				btnGetQueuesClick(Sender);
				Application->ProcessMessages();
				// Fill the IVR Tab if there is IVRAccessInfo in Login Response
				Log("Login Response:\r\n" + dmRestClient->LoginResponse->JSONText);
				// TabControl1->ActiveTab = tabIVR;
				Application->ProcessMessages();
				FillIVRGrid(dmRestClient->LoginResponse->JSONText);

				// Set the Tab to Extensions
				TabControl1->ActiveTab = tabExtensions;
				lblLoginStatus->Text = lblLoginResult->Text + ":" + lblLoginResponse->Text;
				}
			}
		}
	catch (Exception &E)
		{
		Log("::btnSubmitLoginClick - Exception:" + E.Message);
		}
	aniBusy->Enabled = false;
	aniBusy->Visible = false;
	TabControl1->Repaint();
	Application->ProcessMessages();

	}

void __fastcall TfrmRestClient::FillIVRGrid(UnicodeString uJSONText)
	{
	int iCalledIDCol = 0;
	int iDescrCol = 1;
	int iBookingCol = 2;
	int iTripStatusCol = 3;
	int iCancelCol = 4;
	TTMSFMXSwitchGridCell *FSwitch;
	//
	// Get The Columns for each element
	//
	/*
	 if (GetColumnByName(grdIVR, "CalledID", iCalledIDCol) == false)
	 return;
	 if (GetColumnByName(grdIVR, "Description", iDescrCol) == false)
	 return;
	 if (GetColumnByName(grdIVR, "Booking", iBookingCol) == false)
	 return;
	 if (GetColumnByName(grdIVR, "Trip Status", iTripStatusCol) == false)
	 return;
	 if (GetColumnByName(grdIVR, "Cancel", iCancelCol) == false)
	 return;
	 */
	// Get a JSON Object from the uJSONText passed (Response from Login)
	TJSONObject *JRespObj = (TJSONObject*) TJSONObject::ParseJSONValue(TEncoding::ASCII->GetBytes(uJSONText), 0);
	// We should have an array of IVRAccessInfo elements
	TJSONArray *JIVRInfo = (TJSONArray*)JRespObj->Get("IVRAccessInfo")->JsonValue;
	grdEIVR->RowCount = JIVRInfo->Count + 1;
	grdEIVR->AutoSizeColumns(true);
	grdEIVR->Update();
	TJSONObject *IVRItem;

	for (int ii = 1; ii <= JIVRInfo->Count; ii++)
		{
		IVRItem = (TJSONObject*) JIVRInfo->Items[ii - 1];
		// CalledID
		grdEIVR->Cells[iCalledIDCol][ii] = Tools->FormatPhone(IVRItem->GetValue("CalledID")->Value());
		// Description
		grdEIVR->Cells[iDescrCol][ii] = IVRItem->GetValue("FleetName")->Value();
		grdEIVRResize((TObject *)grdEIVR);
		Application->ProcessMessages();
		}

	for (int ii = 1; ii <= JIVRInfo->Count; ii++)
		{
		IVRItem = (TJSONObject*) JIVRInfo->Items[ii - 1];
		// Booking
		SetSwitch(grdEIVR, IVRItem, "Booking", iBookingCol, ii);
		// Trip Status
		SetSwitch(grdEIVR, IVRItem, "TripStatus", iTripStatusCol, ii);
		// Cancel
		SetSwitch(grdEIVR, IVRItem, "Cancel", iCancelCol, ii);
		Application->ProcessMessages();
		}

	// grdEIVR->Repaint();
	}

void __fastcall TfrmRestClient::SetSwitch(TTMSFMXGrid *Grd, TJSONObject *JObj, AnsiString FieldName, int ACol, int ARow)
	{
	TTMSFMXSwitchGridCell *FSwitch = NULL;
	Log("SetSwitch::Col:" + IntToStr(ACol) + " Row:" + IntToStr(ARow));
	try
		{
		FSwitch = (TTMSFMXSwitchGridCell*)grdEIVR->GetCellObject(Cell(ACol, ARow));
		}
	catch (Exception &E)
		{
		// Must not be a switch
		Log("SetSwitch:: GetCellObject Exception:" + E.Message);
		return;
		}

	if (FSwitch == NULL)
		{
		Log("SetSwitch:: GetCellObject is NULL: " + FieldName);
		}

	try
		{
		if (JObj->GetValue(FieldName)->Value() == "True")
			{
			FSwitch->Switch->IsChecked = true;
			FSwitch->Text = "Enabled";
			FSwitch->StyleName = "";
			FSwitch->DisplayText = true;
			FSwitch->Switch->StyleName = "";
			Log("SetSwitch:: Setting " + FieldName + " Col:" + IntToStr(ACol) + " Row:" + IntToStr(ARow) + " True");
			}
		else
			{
			FSwitch->Switch->IsChecked = false;
			FSwitch->Text = "Disabled";
			Log("SetSwitch:: Setting " + FieldName + " Col:" + IntToStr(ACol) + " Row:" + IntToStr(ARow) + " False");
			}
		}
	catch (Exception &E)
		{
		Log("SetSwitch:: GetValue (" + FieldName + ") Exception: " + E.Message);
		}
	}

//
// GetExtensionStatus - Gets the individual Extension Status for each extension in the StringGrid
//
void __fastcall TfrmRestClient::GetExtensionStates()
	{
	int iCol = 0;
	int iStateCol = 0;
	int iStatusCol = 0;
	// Get The Column that has Extension so we can get each element in grid
	if (GetColumnByName(grdExtensions, "Extension", iCol) == false)
		return;
	if (GetColumnByName(grdExtensions, "State", iStateCol) == false)
		return;
	if (GetColumnByName(grdExtensions, "Status", iStatusCol) == false)
		return;

	Log("GetExtensionStates::Token:" + Token);

	// Set the Token for ExtStatusRequest
	dmRestClient->ExtStatusRequest->Params->ParameterByName("Token")->Value = Token;
	// Loop through each row in Grid and send a REST request for that Extension to get its current status
	for (int iRow = 0; iRow < grdExtensions->RowCount; iRow++)
		{
		Application->ProcessMessages();
		dmRestClient->ExtStatusRequest->Params->ParameterByName("Extension")->Value = grdExtensions->Cells[iCol][iRow];
		try
			{
			dmRestClient->ExtStatusRequest->Execute();
			}
		catch (Exception &E)
			{
			Log("dmRestClient->ExtStatusRequest->Execute(): Exception:" + E.Message);
			// Black button with Red X
			grdExtensions->Cells[iStatusCol][iRow] = 8;
			}
		// Successful response from REST Service
		if (dmRestClient->ExtStatusResponse->Status.Success())
			{
			// The Extension was found so Result = Success
			if (dmRestClient->ExtStatusMemTable->Fields->FieldByName("ResultCode")->AsString == "Success")
				{
				// Set the Text of the Cell to the State
				grdExtensions->Cells[iStateCol][iRow] = dmRestClient->ExtStatusMemTable->Fields->FieldByName("Status")
					 ->AsString + " - " + dmRestClient->ExtStatusMemTable->Fields->FieldByName("StatusText")->AsString;
				switch (StrToInt(dmRestClient->ExtStatusMemTable->Fields->FieldByName("Status")->AsString))
					{
					// Extension Hint removed
				case -1:
					grdExtensions->Cells[iStatusCol][iRow] = 6;
					break;
					// Extension Removed
				case -2:
					grdExtensions->Cells[iStatusCol][iRow] = 7;
					break;
					// Idle
				case 0:
					grdExtensions->Cells[iStatusCol][iRow] = 0;
					break;
					// InUse
				case 1:
					grdExtensions->Cells[iStatusCol][iRow] = 1;
					break;
					// Busy
				case 2:
					grdExtensions->Cells[iStatusCol][iRow] = 2;
					break;
					// Unavailable
				case 4:
					grdExtensions->Cells[iStatusCol][iRow] = 3;
					break;
					// Ringing
				case 8:
					grdExtensions->Cells[iStatusCol][iRow] = 4;
					break;
					// On Hold
				case 16:
					grdExtensions->Cells[iStatusCol][iRow] = 5;
					break;

					// Otherwise check the bit state
				default:

					break;
					}
				}
			}
		}
	}

bool __fastcall TfrmRestClient::GetColumnByName(TStringGrid *Grd, AnsiString ColumnName, int &ColumnIndex)
	{
	int OriginalColumnIndex = ColumnIndex;
	if (Grd == NULL)
		return (false);

	for (ColumnIndex = 0; ColumnIndex < Grd->ColumnCount; ColumnIndex++)
		{
		if (Grd->ColumnByIndex(ColumnIndex)->Header.Trim() == ColumnName)
			return (true);
		}
	ColumnIndex = OriginalColumnIndex;
	return (false);
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnGetQueuesClick(TObject *Sender)
	{
	int Available;

	// find the center of the screen
	aniQueues->Position->X = (int)(this->Width - (int)aniQueues->Width) / 2;
	aniQueues->Position->Y = (int)(this->Height - (int)aniQueues->Height) / 2;
	aniQueues->Enabled = true;
	aniQueues->Visible = true;
	Application->ProcessMessages();
	try
		{
		if (IsLoggedIn == false)
			{
			btnSubmitLoginClick(Sender);
			}

		if (IsLoggedIn == false)
			{
			aniQueues->Enabled = false;
			aniQueues->Visible = false;
			ShowMessage("You are not logged in");
			return;
			}

		dmRestClient->QueStatusRequest->Params->ParameterByName("Token")->Value = Token;
		dmRestClient->QueStatusRequest->Client->BaseURL = edtBaseURL->Text;
		try
			{
			dmRestClient->QueStatusRequest->Execute();
			}
		catch (Exception &E)
			{
			Log("QueueStatusRequest Exception:" + E.Message);
			Log("Response:" + dmRestClient->QueStatusResponse->Content);
			}
		if (dmRestClient->QueStatusResponse->Status.Success())
			{
			if (dmRestClient->QueStatusMemTable->Fields->FieldByName("Result")->AsString == "Success")
				{
				// Parse the QueueParms from field in QueStatusMemTable
				// Entire Response
				TJSONObject * JResponseObj = (TJSONObject*) TJSONObject::ParseJSONValue
					 (TEncoding::ASCII->GetBytes(dmRestClient->QueStatusResponse->Content), 0);
				// Should be Array of QueueParams
				TJSONArray *JQParamArray = (TJSONArray*)JResponseObj->Get("QueueParams")->JsonValue;

				grdQue->RowCount = JQParamArray->Count;
				for (int ii = 0; ii < JQParamArray->Count; ii++)
					{
					//
					// Load the Grid with Ques
					//
					TJSONObject *QParam = (TJSONObject*) JQParamArray->Items[ii];
					int iCol = 0;
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Queue")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Description")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Max")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Strategy")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Calls")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Holdtime")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("TalkTime")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Completed")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Abandoned")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("ServiceLevel")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("ServicelevelPerf")->Value();
					grdQue->Cells[iCol++][ii] = QParam->GetValue("Weight")->Value();
					grdQue->Cells[iCol][ii] = "0"; // Members
					Available = 0;
					//
					// There may be an array of QueueMember objects
					//
					if (QParam->GetValue("QueueMember") != NULL)
						{
						//
						// Get the Members of this Que
						//
						TJSONArray *JQMemberArray = (TJSONArray*) TJSONObject::ParseJSONValue
							 (TEncoding::ASCII->GetBytes(QParam->GetValue("QueueMember")->ToJSON()), 0);
						//
						// Set the Count of Members
						//
						grdQue->Cells[iCol++][ii] = IntToStr(JQMemberArray->Count);
						//
						// This loops through Members
						//
						//
						// Status - The numeric device state status of the queue member.
						// 0 - AST_DEVICE_UNKNOWN
						// 1 - AST_DEVICE_NOT_INUSE
						// 2 - AST_DEVICE_INUSE
						// 3 - AST_DEVICE_BUSY
						// 4 - AST_DEVICE_INVALID
						// 5 - AST_DEVICE_UNAVAILABLE
						// 6 - AST_DEVICE_RINGING
						// 7 - AST_DEVICE_RINGINUSE
						// 8 - AST_DEVICE_ONHOLD

						for (int jj = 0; jj < JQMemberArray->Count; jj++)
							{
							TJSONObject *QMember = (TJSONObject*)JQMemberArray->Items[jj];
							memLog->Lines->Add("Member:" + QMember->GetValue("Name")->Value());
							memLog->Lines->Add("Location:" + QMember->GetValue("Location")->Value());
							memLog->Lines->Add("StateInterface:" + QMember->GetValue("StateInterface")->Value());
							memLog->Lines->Add("Status:" + QMember->GetValue("Status")->Value());
							if (QMember->GetValue("Status")->Value() == "1")
								Available++;
							}
						grdQue->Cells[iCol++][ii] = IntToStr(Available);

						}
					}
				SizeGridColumns(grdQue);
				}
			}
		}
	catch (Exception &E)
		{
		Log("GetQueues:" + E.Message);
		}
	aniQueues->Enabled = false;
	aniQueues->Visible = false;
	btnGetQueues->ResetFocus();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbQueuesClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabQueues;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnClearLogClick(TObject *Sender)
	{
	memLog->Lines->Clear();
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::lbIVRClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabIVR;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnResetStatsClick(TObject *Sender)
	{
	try
		{
		if (IsLoggedIn == false)
			{
			btnSubmitLoginClick(Sender);
			}

		if (IsLoggedIn == false)
			{
			ShowMessage("You are not logged in");
			return;
			}

		dmRestClient->QueResetStatsRequest->Params->ParameterByName("Token")->Value = Token;
		dmRestClient->QueResetStatsRequest->Execute();
		if (dmRestClient->QueResetStatsResponse->Status.Success())
			{
			// Update the Que Stats
			btnGetQueuesClick(Sender);
			}
		else
			{
			// failed
			}
		}
	catch (Exception &E)
		{

		}
	btnResetStats->ResetFocus();
	// TabControl1->Repaint();
	// Application->ProcessMessages();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::IOHandlerStackStatus(TObject *ASender, const TIdStatus AStatus,
	 const UnicodeString AStatusText)
	{
	Log("TfrmRestClient::IOHandlerStack Status:" + AStatusText);
	switch (AStatus)
		{
	case hsResolving:
		break;
	case hsConnecting:
		btnJSONConnect->Text = "Connecting";
		break;
	case hsConnected:
		btnJSONConnect->Text = "Disconnect";
		break;
	case hsDisconnecting:
		btnJSONConnect->Text = "Disconnecting";
		break;
	case hsDisconnected:
		btnJSONConnect->Text = "Connect";
		break;
	case hsStatusText:
		break;
	case ftpTransfer:
		break;
	case ftpReady:
		break;
	case ftpAborted:
		break;
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPInterceptConnect(TIdConnectionIntercept *ASender)
	{
	Log("TfrmRestClient::TCPInterceptConnect");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPInterceptDisconnect(TIdConnectionIntercept *ASender)
	{
	// This gets called after memSettings is destroyed
	Log("TfrmRestClient::TCPInterceptDisconnect");
	if (btnJSONConnect != NULL)
		btnJSONConnect->Text = "Connect";
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPInterceptReceive(TIdConnectionIntercept *ASender, TIdBytes &ABuffer)
	{
	// Log("TCPInterceptReceive");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPInterceptSend(TIdConnectionIntercept *ASender, TIdBytes &ABuffer)
	{
	Log("TfrmRestClient::TCPInterceptSend");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientConnected(TObject *Sender)
	{
	UnicodeString UOut = "Events:";
	Log("TfrmRestClient::TCPClientConnected Event:");
	lblSettingsStatus->Text = GetCurrentDateTimeMS(L"hh:nn:ss") + " Connected:";
	OnConnectionStatusChange(Sender);
	if (TCPClient->Connected() == true)
		{
		Log("TCPClient->Connected() is TRUE");
		// Send the Connection the Events and Priviliges that we care about
		UOut += "ExtensionStatus,";
		UOut += "QueueCallerJoin,";
		UOut += "QueueCallerLeave,";
		UOut += "QueueMemberStatus,";
		UOut += "DeviceStateChange,";
		UOut += "Hangup,";
		UOut += "AgentCalled,";
		UOut += "AgentCalled,";
		UOut += "AgentComplete,";
		UOut += "AgentConnect,";
		UOut += "Newstate,";
		UOut += "QueueCallerAbandon\r\n";
		UOut += "Privileges:*\r\n\r\n";
		Log("Sending:\r\n" + UOut);
		TCPClient->Socket->Write(UOut);
		}
	else
		Log("TCPClient->Connected() is FALSE");

	// ------------------------------------------
	// If the thread is already running - Kill it
	// ------------------------------------------
	if (Thrd != (TSocketReaderThread*)NULL)
		{
		if (Thrd->Started)
			{
			Log("TCPClientConnected::Terminating Thread");
			Thrd->Terminate();
			}
		delete Thrd;
		}

	Thrd = (TSocketReaderThread*)NULL;
	try
		{
		Thrd = new TSocketReaderThread(true);
		Thrd->TCPClient = TCPClient;
		Thrd->Memo = memLog;
		Thrd->CounterLabel = lblBytesReceived;
		Thrd->QueGrid = grdQue;
		Thrd->ExtGrid = grdExtensions;
		Thrd->LogOutput = swtLogOutput;
		lblBytesReceived->Text = "0";
		Thrd->OnTerminate = OnThreadTerminate;
		Thrd->Start();
		tmrKeepAlive->Enabled = true;
		}
	catch (Exception &E)
		{
		Log("Exception setting Thrd->Socket" + E.Message);
		if (Thrd)
			{
			if (Thrd->Started)
				{
				Log("TCPClientConnected::Terminating Thread - due to Exeption");
				Thrd->Terminate();
				}
			}

		}

	btnJSONConnect->Text = "Disconnect";
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientDisconnected(TObject *Sender)
	{
	Log("TfrmRestClient::TCPClientDisConnected");
	lblSettingsStatus->Text = GetCurrentDateTimeMS(L"hh:nn:ss") + " Disconnected:";
	OnConnectionStatusChange(Sender);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientStatus(TObject *ASender, const TIdStatus AStatus,
	 const UnicodeString AStatusText)
	{
	Log("TfrmRestClient::TCPClientStatus:");
	switch (AStatus)
		{
	case hsResolving:
		Log("Resolving");
		break;
	case hsConnecting:
		btnJSONConnect->Text = "Connecting";
		Log("Connecting");
		break;
	case hsConnected:
		Log("Connected");
		ledJSONConnection->State = true;
		ToolBarLabel->FontColor = ledJSONConnection->OnColor;
		gleToolBarLabel->GlowColor = ledJSONConnection->OnColor;
		frmRestClient->SystemStatusBar->BackgroundColor = ledJSONConnection->OnColor;
		break;
	case hsDisconnecting:
		btnJSONConnect->Text = "Disconnecting";
		Log("Disconnecting");
		break;
	case hsDisconnected:
		Log("Disconnected");
		btnJSONConnect->Text = "Connect";
		ledJSONConnection->State = false;
		ToolBarLabel->FontColor = ledJSONConnection->OffColor;
		gleToolBarLabel->GlowColor = ledJSONConnection->OffColor;
		frmRestClient->SystemStatusBar->BackgroundColor = ledJSONConnection->OffColor;
		break;
	case hsStatusText:
		Log("StatusText");
		break;
	case ftpTransfer:
		Log("ftpTransfer");
		break;
	case ftpReady:
		Log("ftpReady");
		break;
	case ftpAborted:
		Log("Aborted");
		break;
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientWork(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount)
	{
	UStr = "TfrmRestClient::TCPClientWork:";
	switch (AWorkMode)
		{
	case wmRead:
		UStr += UStr.sprintf(L"%s:wmRead - WorkCount(%ld)", UStr.c_str(), AWorkCount);
		break;
	case wmWrite:
		UStr += UStr.sprintf(L"%s:wmWrite - WorkCount(%ld)", UStr.c_str(), AWorkCount);
		break;
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientWorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax)
	{
	Log("TfrmRestClient::TCPClientWorkBegin");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPClientWorkEnd(TObject *ASender, TWorkMode AWorkMode)
	{
	Log("TfrmRestClient::TCPClientWorkEnd");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::OnThreadTerminate(TObject *Sender)
	{
	Log("TfrmRestClient::OnThreadTerminate");
	Thrd = (TSocketReaderThread*)NULL;
	if (btnJSONConnect != NULL)
		btnJSONConnect->Text = "Connect";
	if (ledJSONConnection != NULL)
		{
		OnConnectionStatusChange(Sender);
		}
	tmrKeepAlive->Enabled = false;
	}

void __fastcall TfrmRestClient::Log(UnicodeString asOut)
	{
	asOut = GetCurrentDateTimeMS(L"hh:NN:ss") + ":" + asOut;

	if (memLog != NULL)
		{
		memLog->Lines->Add(asOut);
		memLog->Repaint();
		Application->ProcessMessages();
		}
	}

void __fastcall TfrmRestClient::GCLog(UnicodeString asOut)
	{
	asOut = GetCurrentDateTimeMS(L"hh:NN:ss") + ":" + asOut;

	if (memGC != NULL)
		{
		memGC->Lines->Add(asOut);
		memGC->Repaint();
		Application->ProcessMessages();
		}
	}

UnicodeString __fastcall TfrmRestClient::GetCurrentDateTimeMS(UnicodeString FmtString)
	{
	UnicodeString USNow = "";
	unsigned short sHH, sMM, sSS, sMS;
	TDateTime DTNow = Now();
	DTNow.DecodeTime(&sHH, &sMM, &sSS, &sMS);
	USNow = DTNow.FormatString(FmtString) + USNow.sprintf(L".%.3d", sMS);
	return (USNow);
	}

void __fastcall TfrmRestClient::btnJSONConnectClick(TObject *Sender)
	{
	Log("TAsteriskAppForm::btnJSONConnectClick");
	tmrKeepAlive->Enabled = false;
	lblSettingsStatus->Text = "";

	Log("  TCPClient->Connected = " + AnsiString((ledJSONConnection->State ? "true" : "false")));

	if (btnJSONConnect->Text == "Connect" || ledJSONConnection->State == false)
		{
		if (ledJSONConnection->State == false)
			Log("TCPClient->Connected() is FALSE");
		lblSettingsStatus->Text = "Connecting to " + cbxHostAddress->Text.Trim() + ":" + cbxHostPort->Text.Trim();
		Log(lblSettingsStatus->Text);

		try
			{
			TCPClient->Disconnect();
			}
		catch (Exception &E)
			{
			Log("Exception: Disconnecting  " + cbxHostAddress->Text.Trim() + ":" + cbxHostPort->Text.Trim());
			}
		TCPClient->Host = cbxHostAddress->Text.Trim();
		TCPClient->Port = (short)cbxHostPort->Text.ToInt();
		TCPClient->ConnectTimeout = 10000;
		try
			{
			TCPClient->Connect();
			}
		catch (EIdConnectTimeout &TO)
			{
			Log("TCPClient->Connection Timeout");
			lblSettingsStatus->Text = GetCurrentDateTimeMS(L"hh:NN:ss") + ":TPClient->Connection Timeout";
			btnJSONConnect->Text = "Connect";
			}
		catch (Exception &E)
			{
			Log("TCPClient->Connect Exception:" + E.Message);
			lblSettingsStatus->Text = GetCurrentDateTimeMS(L"hh:NN:ss") + ":TPClient->Connect:" + E.Message;
			btnJSONConnect->Text = "Connect";
			}
		}
	else
		{
		try
			{
			Log("Disconnecting");
			lblSettingsStatus->Text = GetCurrentDateTimeMS(L"hh:NN:ss") + ":TPClient->Disconnecting";
			TCPClient->Disconnect();
			OnConnectionStatusChange(Sender);
			}
		catch (Exception &E)
			{
			Log("TCPClient->Disconnect Exception:" + E.Message);
			OnConnectionStatusChange(Sender);
			}
		}
	}

//
// OnConnectionStatusChange - Handles all of the controls that indicate the connection status
//
void __fastcall TfrmRestClient::OnConnectionStatusChange(TObject *Sender)
	{
	try
		{
		if (TCPClient != NULL)
			{
			if (TCPClient->Connected())
				{
				btnJSONConnect->Text = "Disconnect";
				ledJSONConnection->State = true;
				ToolBarLabel->FontColor = ledJSONConnection->OnColor;
				gleToolBarLabel->GlowColor = ledJSONConnection->OnColor;
				frmRestClient->SystemStatusBar->BackgroundColor = ledJSONConnection->OnColor;
				}
			else
				{
				btnJSONConnect->Text = "Connect";
				ledJSONConnection->State = false;
				ToolBarLabel->FontColor = ledJSONConnection->OffColor;
				gleToolBarLabel->GlowColor = ledJSONConnection->OffColor;
				frmRestClient->SystemStatusBar->BackgroundColor = ledJSONConnection->OffColor;
				}
			}
		}
	catch (Exception &E)
		{
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::FormClose(TObject *Sender, TCloseAction &Action)
	{
	// Cleanup the thread
	if (Thrd != (TSocketReaderThread*)NULL)
		{
		try
			{
			if (Thrd->Started)
				{
				Log("Terminating Thread");
				Thrd->Terminate();
				}
			}
		catch (...)
			{
			}
		}
	}

// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::ListView1PullRefresh(TObject *Sender)
	{
	GetExtensionStates();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::expUserManagerClick(TObject *Sender)
	{
	((TExpander*)Sender)->IsExpanded = ((TExpander*)Sender)->IsExpanded ? false : true;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::lbEditExtensionClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabExtEdit;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::MultiView1Shown(TObject *Sender)
	{
	MultiView1->Enabled = true;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::Button1Click(TObject *Sender)
	{
	UnicodeString USNow;
	// Clear the Extensions Grid
	dmRestClient->ExtMemTable->Active = false;
	// Clear Login Output
	lblToken->Text = "";
	lblLoginResponse->Text = "";
	lblLoginErrorMsg->Text = "";
	lblLoginResult->Text = "";
	lblLoginTime->Text = "";
	lblTimeStamp->Text = "";
	lblTenantName->Text = "";
	// Execute the Login REST request
	Application->ProcessMessages();
	lblLoginStatus->Text = "Logging In";
	try
		{
		dmRestClient->LoginRequest->Execute();
		}
	catch (Exception &E)
		{
		Log("dmRestClient->LoginRequest->Execute():Exception:" + E.Message);
		Log("Content:" + dmRestClient->LoginResponse->Content);
		aniBusy->Enabled = false;
		aniBusy->Visible = false;
		TabControl1->Repaint();
		Application->ProcessMessages();
		return;
		}

	Application->ProcessMessages();
	// Check if request was successful as far as going to the REST Service
	if (dmRestClient->LoginResponse->Status.Success())
		{
		// Login Success
		if (dmRestClient->LoginMemTable->Fields->FieldByName("Result")->AsString == "Success")
			{
			// Set the LoginTime
			unsigned short sHH, sMM, sSS, sMS;
			TDateTime DTNow = Now();
			DTNow.DecodeTime(&sHH, &sMM, &sSS, &sMS);
			lblLoginTime->Text = DTNow.FormatString("YYYY-mm-dd h:nn:ss") + USNow.sprintf(L".%.3d", sMS);
			// Set the Token
			Token = dmRestClient->LoginMemTable->Fields->FieldByName("Token")->AsString;
			IsLoggedIn = true;
			lblLoginStatus->Text = "Getting extension list";
			}

		// Fill the IVR Tab if there is IVRAccessInfo in Login Response
		Log("Login Response:\r\n" + dmRestClient->LoginResponse->JSONText);
		TabControl1->ActiveTab = tabIVR;
		Application->ProcessMessages();
		FillIVRGrid(dmRestClient->LoginResponse->JSONText);

		}

	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::CalloutPanel1Click(TObject *Sender)
	{
	CalloutPanel1->Visible = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::ListBox3ChangeCheck(TObject *Sender)
	{
	lblSelected->Text = "";
	for (int ii = 0; ii < ListBox3->Items->Count; ii++)
		{
		if (ListBox3->ListItems[ii]->IsChecked)
			{
			lblSelected->Text = lblSelected->Text + "[" + ListBox3->Items->Strings[ii] + "] ";
			}
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::SpeedButton3Click(TObject *Sender)
	{
	CalloutPanel1->Visible = CalloutPanel1->Visible ? false : true;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::SpeedButton2Click(TObject *Sender)
	{
	// find the center of the screen
	aniExtensions->Position->X = (int)(this->Width - (int)aniBusy->Width) / 2;
	aniExtensions->Position->Y = (int)(this->Height - (int)aniBusy->Height) / 2;
	aniExtensions->Enabled = true;
	aniExtensions->Visible = true;
	Application->ProcessMessages();
	// Check if request was successful as far as going to the REST Service
	if (dmRestClient->LoginResponse->Status.Success())
		{
		// Login Success
		if (dmRestClient->LoginMemTable->Fields->FieldByName("Result")->AsString == "Success")
			{
			// Set the Token
			Token = dmRestClient->LoginMemTable->Fields->FieldByName("Token")->AsString;
			IsLoggedIn = true;
			// Get the Extension list
			btnRetrieveClick(Sender);
			SpeedButton2->ResetFocus();
			SpeedButton2->Repaint();
			// Get Each Extensions Current Status
			GetExtensionStates();
			}
		}
	aniExtensions->Enabled = false;
	aniExtensions->Visible = false;
	// SpeedButton2->ResetFocus();
	// ect);
	tabExtensions->SetFocus();
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::tmrKeepAliveTimer(TObject *Sender)
	{
	try
		{

		if (TCPClient->Connected())
			TCPClient->Socket->WriteLn("\r\n");
		Log("KeepAlive Sent:");
		}
	catch (Exception &E)
		{
		//
		// May want to try to reconnect here
		//
		Log("KeepAlive Exception:" + E.Message);
		tmrKeepAlive->Enabled = false;
		if (Thrd != NULL)
			Thrd->Terminate();
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::MultiView1Hidden(TObject *Sender)
	{
	MultiView1->Enabled = false;
	}

// ---------------------------------------------------------------------------
// SizeGridColumns - This takes a filled String Grid and sets the Width of
// all columns based on the text length. The minimum size is the Header Text size
// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::SizeGridColumns(TStringGrid *Grid)
	{
	float ColWidth;
	float TextWidth;
	float Padding = 8.0f;
	// Android Note: typeid(...) throws exception and does not work
	// Disabled checking for Type = TStringColumn
	// UnicodeString ColType;
	// UnicodeString StrType = typeid(TStringColumn).name();

	// Size the Grid based on the Header Names as a minimum
	for (int iCol = 0; iCol < Grid->ColumnCount; iCol++)
		{
		Grid->Columns[iCol]->Width = Grid->Columns[iCol]->Canvas->TextWidth(Grid->Columns[iCol]->Header.Trim()) + Padding;
		}

	// Now make sure there is room for the Data in the Grid
	for (int iCol = 0; iCol < Grid->ColumnCount; iCol++)
		{
		// ColType = typeid(*Grid->Columns[iCol]).name();
		Application->ProcessMessages();
		// make sure Column type is TStringColumn
		// if (ColType == StrType)
		// {
		for (int iRow = 0; iRow < Grid->RowCount; iRow++)
			{
			Application->ProcessMessages();
			ColWidth = Grid->Columns[iCol]->Width;
			TextWidth = Grid->Columns[iCol]->Canvas->TextWidth(Grid->Cells[iCol][iRow]);
			if (TextWidth + Padding > ColWidth)
				Grid->Columns[iCol]->Width = TextWidth + Padding;
			}
		// }
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::swtLogOutputSwitch(TObject *Sender)
	{
	if (swtLogOutput->IsChecked)
		lblLogOutput->Text = "On";
	else
		lblLogOutput->Text = "Off";
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::swLoginSwitch(TObject *Sender)
	{
	if (swLogin->IsChecked)
		lblAutoConnect->Text = "On";
	else
		lblAutoConnect->Text = "Off";
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::grdEIVRResize(TObject *Sender)
	{
	// Resize the IVR Grid
	float ColWidth;
	float TextWidth;
	float Padding = 8.0f;
	Log("grdEIVRResize::");
	// if (true) return;
	TTMSFMXGridColumn *Col;

	// Size the Grid based on the Header Names as a minimum
	for (int iCol = 0; iCol < grdEIVR->ColumnCount; iCol++)
		{
		Col = grdEIVR->Columns->Items[iCol];
		if (Col->ColumnType != ctSwitch)
			{
			if (Col->Width < grdEIVR->Canvas->TextWidth(grdEIVR->Cells[iCol][0]) + Padding)
				Col->Width = grdEIVR->Canvas->TextWidth(grdEIVR->Cells[iCol][0]) + Padding;
			}
		}

	// Now make sure there is room for the Data in the Grid
	for (int iCol = 0; iCol < grdEIVR->Columns->Count; iCol++)
		{
		// Application->ProcessMessages();
		Col = grdEIVR->Columns->Items[iCol];
		if (Col->ColumnType != ctSwitch)
			{
			for (int iRow = 0; iRow < grdEIVR->RowCount; iRow++)
				{
				// Application->ProcessMessages();
				TextWidth = grdEIVR->Canvas->TextWidth(grdEIVR->Cells[iCol][iRow]) + Padding;
				if (TextWidth > Col->Width)
					Col->Width = TextWidth;
				}
			}
		else
			{
			Col->Width = grdEIVR->DefaultColumnWidth;
			}
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::grdEIVRCellSwitchClick(TObject *Sender, int ACol, int ARow, TFmxObject *Cell)
	{
	TTMSFMXSwitchGridCell *FSwitch;

	FSwitch = (TTMSFMXSwitchGridCell*)Cell;
	/*
	 {
	 lblIVRText->Text = "IVRCellSwitchClick: GetCellObject is NULL";
	 return;
	 }
	 */
	if (TabControl1->ActiveTab == tabIVR)
		{
		// Switch was clicked update the IVR settings via REST
		lblIVRText->Text = "IVRCellSwitchClick: CalledID:" + grdEIVR->Cells[0][ARow] + " Fleet:" + grdEIVR->Cells[1][ARow];

		switch (ACol)
			{
			// Booking
		case 2:
			if (FSwitch->Switch->IsChecked)
				lblIVRText->Text = lblIVRText->Text + " Booking is Enabled";
			else
				lblIVRText->Text = lblIVRText->Text + " Booking is Disabled";
			break;
			// TripStatus
		case 3:
			if (FSwitch->Switch->IsChecked)
				lblIVRText->Text = lblIVRText->Text + " TripStatus is Enabled";
			else
				lblIVRText->Text = lblIVRText->Text + " TripStatus is Disabled";
			break;
			// Cancel
		case 4:
			if (FSwitch->Switch->IsChecked)
				lblIVRText->Text = lblIVRText->Text + " Cancel is Enabled";
			else
				lblIVRText->Text = lblIVRText->Text + " Cancel is Disabled";
			break;
			}
		lblIVRText->Repaint();
		}
	Application->ProcessMessages();
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::grdEIVRColumnSize(TObject *Sender, int ACol, float &NewWidth)
	{
	Log("::grdEIVRColumnSize");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::grdEIVRColumnSized(TObject *Sender, int ACol, float NewWidth)
	{
	Log("::grdEIVRColumnSized");
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnGCGetCustomerClick(TObject *Sender)
	{
	TTime StartTime;
	UnicodeString uTmp;

	TJSONObject *oAddr;
	//
	// Clear all buttons and text
	//

	lblTSStatus->Text = "";
	// Info
	lblGCITimeStamp->Text = "";
	lblGCICommand->Text = "";
	lblGCIActionID->Text = "";
	lblGCIResult->Text = "";
	lblGCIResultCode->Text = "";
	lblGCIErrorMsg->Text = "";
	lblGCIVehicleNo->Text = "";
	lblGCIAccountNo->Text = "";
	lblGCIAccountName->Text = "";
	lblGCICustomerNo->Text = "";
	lblGCICellPhone->Text = "";
	lblGCITripID->Text = "";
	lblGCIDispatchUniqueID->Text = "";
	lblGCIFleet->Text = "";
	// Clear Pickup Data
	edtGCPAccessCode->Text = "";
	edtGCPAddressID->Text = "";
	edtGCPApartment->Text = "";
	edtGCPBuilding->Text = "";
	edtGCPCity->Text = "";
	edtGCPLatitude->Text = "";
	edtGCPLongitude->Text = "";
	edtGCPName->Text = "";
	edtGCPStreet->Text = "";
	edtGCPStreetNo->Text = "";
	edtGCPZip->Text = "";
	// Clear Dropoff Data
	edtGCDAccessCode->Text = "";
	edtGCDAddressID->Text = "";
	edtGCDApartment->Text = "";
	edtGCDBuilding->Text = "";
	edtGCDCity->Text = "";
	edtGCDLatitude->Text = "";
	edtGCDLongitude->Text = "";
	edtGCDName->Text = "";
	edtGCDStreet->Text = "";
	edtGCDStreetNo->Text = "";
	edtGCDZip->Text = "";
	// Clear Misc Data
	cbxMRemarks->Items->Clear();
	lblMIsDuplicate->Text = "";
	lblMIsBlocked->Text = "";
	lblMRebuiltTime->Text = "";
	lblMLastUpdate->Text = "";
	// Clear out Address Components
	cbxAddresses->Items->Clear();
	lblADAddressIndex->Text = "";
	lblADName->Text = "";
	lblADLatitude->Text = "";
	lblADLongitude->Text = "";
	lblADDispatchZone->Text = "";
	lblADMapNumber->Text = "";
	lblADMapCoordinates->Text = "";
	lblADZipCode->Text = "";
	lblADStreetNumber->Text = "";
	lblADStreetName->Text = "";
	lblADCityName->Text = "";
	lblADBuildingName->Text = "";
	lblADApartment->Text = "";
	lblADAccessCode->Text = "";
	// Clear BookOrder data
	cbxBOAddresses->Items->Clear();
	lblBOName->Text = "";
	lblBOLatitude->Text = "";
	lblBOLongitude->Text = "";
	lblBODispatchZone->Text = "";
	lblBOMapNo->Text = "";
	lblBOMapCoords->Text = "";
	lblBOZipCode->Text = "";
	lblBOStreetNo->Text = "";
	lblBOStreetName->Text = "";
	lblBOCityName->Text = "";
	lblBOBuildingName->Text = "";
	lblBOApartment->Text = "";
	lblBOAccessCode->Text = "";
	// Disable the BookOrder button
	btnBookOrder->Enabled = false;
	btnBOCancel->Enabled = false;
	//
	// Check the memGC so we dont overflow
	//
	memGCChange(Sender);
	//
	// Build the REST Request from the edtHost + Port
	//
	dmRestClient->IVRClient->BaseURL = "HTTP://" + edtHost->Text + ":5005/IVRMgr?Command=GetCustomerInfo";
	// Set the Parameters
	dmRestClient->IVRGetCustRequest->Params->ParameterByName("CalledID")->Value = edtGCCalledID->Text;
	dmRestClient->IVRGetCustRequest->Params->ParameterByName("CallerID")->Value = edtGCCallerID->Text;
	dmRestClient->IVRGetCustRequest->Params->ParameterByName("UniqueID")->Value = edtGCUniqueID->Text;
	StartTime = Time();
	// Execute the Request
	try
		{
		dmRestClient->IVRGetCustRequest->Execute();
		lblGCIResponse->Text = FloatToStr(MilliSecondsBetween(StartTime, Time())) + " ms";
		lblGCILength->Text = IntToStr((int)dmRestClient->IVRGetCustResponse->ContentLength) + " bytes";
		memGC->Lines->Add(dmRestClient->IVRGetCustResponse->Content);
		}
	catch (Exception &E)
		{
		lblGCStatus->Text = E.Message;
		lblGCIErrorMsg->Text = E.Message;
		lblGCIResponse->Text = FloatToStr(MilliSecondsBetween(StartTime, Time())) + " ms";
		lblGCILength->Text = "0 bytes";
		memDebug->Lines->Add("IVRGetCustRequest Exception:" + E.Message);
		memGC->Lines->Add(dmRestClient->IVRGetCustResponse->Content);
		return;
		}

	//
	// Check for Success
	//
	try
		{
		if (dmRestClient->IVRGetCustRequest->Response->Status.Success())
			{
			lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRGetCustRequest->Response->StatusCode) + ") ";
			lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRGetCustRequest->Response->StatusText;
			lblGCStatus->Text = lblGCStatus->Text + " Response Time:" + lblGCIResponse->Text;
			lblGCStatus->Text = lblGCStatus->Text + " Bytes received:" + lblGCILength->Text;

			}
		else
			{
			lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRGetCustRequest->Response->StatusCode) + ") ";
			lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRGetCustRequest->Response->StatusText;
			lblGCIErrorMsg->Text = lblGCStatus->Text;
			return;
			}
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("Check for Success Exception:" + E.Message + ") ");
		return;
		}

	// Request was successful show the JSON Response
	try
		{
		// This happens if there are special characters in the JSON response
		// "Remarks":"This is a problem "DoubleQuotes""
		if (dmRestClient->IVRGetCustResponse->JSONValue == NULL)
			{
			memGC->Lines->Add("JSONValue is NULL: Invalid JSON Response Look at the response");
			memGC->Lines->Add("-- JSONText:<<<" + dmRestClient->IVRGetCustResponse->JSONText + ">>>");
			memGC->Lines->Add("-- Content:++++" + dmRestClient->IVRGetCustResponse->Content + "++++");
			return;
			}
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("JSONText Exception:" + E.Message);
		memDebug->Lines->Add(dmRestClient->IVRGetCustResponse->Content);
		return;
		}

	// Get a JSON Object from the Response so we can access the elements
	TJSONObject *JRespObj = NULL;
	try
		{
		JRespObj = (TJSONObject*) TJSONObject::ParseJSONValue
			 (TEncoding::ASCII->GetBytes(dmRestClient->IVRGetCustResponse->JSONText), 0);
		//
		// Info data for all responses
		//
		lblGCITimeStamp->Text = GetNonNull(JRespObj, "TimeStamp", "");
		lblGCICommand->Text = GetNonNull(JRespObj, "Command", "");
		lblGCIActionID->Text = GetNonNull(JRespObj, "ActionID", "");
		lblGCIResult->Text = GetNonNull(JRespObj, "Result", "");
		lblGCIResultCode->Text = GetNonNull(JRespObj, "ResultCode", " ");
		lblGCIErrorMsg->Text = GetNonNull(JRespObj, "ErrorMsg", "");

		//
		// Error Response from REST Service (Rider Not Found)
		//
		if (lblGCIResult->Text != "Success")
			{
			lblGCStatus->Text = lblGCStatus->Text + " :" + lblGCIErrorMsg->Text;
			lblGCIVehicleNo->Text = "";
			lblGCIAccountNo->Text = "";
			lblGCIAccountName->Text = "";
			lblGCICustomerNo->Text = "";
			lblGCICellPhone->Text = "";
			lblGCITripID->Text = "";
			lblGCIDispatchUniqueID->Text = "";
			lblGCIFleet->Text = "";
			return;
			}
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("JRespObj Exception:" + E.Message);
		return;
		}
	//
	// If the Response was success these fields are available in JResp
	//
	try
		{
		lblGCIVehicleNo->Text = GetNonNull(JRespObj, "VehicleNo", " ");
		lblGCIAccountNo->Text = GetNonNull(JRespObj, "AccountNo", " ");
		lblGCIAccountName->Text = GetNonNull(JRespObj, "AccountName", " ");
		lblGCICustomerNo->Text = GetNonNull(JRespObj, "CustomerNo", " ");
		lblGCICellPhone->Text = GetNonNull(JRespObj, "CellPhone", " ");
		lblGCITripID->Text = GetNonNull(JRespObj, "TripID", " ");
		lblGCIDispatchUniqueID->Text = GetNonNull(JRespObj, "DispatchUniqueID", " ");
		lblGCIFleet->Text = GetNonNull(JRespObj, "Fleet", " ");
		// BookOrder page

		lblBOFleet->Text = GetNonNull(JRespObj, "Fleet", "");
		lblBOAccountNo->Text = GetNonNull(JRespObj, "AccountNo", "");
		lblBOPhoneNo->Text = GetNonNull(JRespObj, "Phone", GetNonNull(JRespObj, "CellPhone", ""));
		// Default to Cash
		radBOCash->IsChecked = true;
		if (GetNonNull(JRespObj, "PaymentType", "").UpperCase() == "CREDIT")
			radBOCredit->IsChecked = true;
		if (GetNonNull(JRespObj, "PaymentType", "").UpperCase() == "ACCOUNT")
			radBOAccount->IsChecked = true;
		lblBOCustomerNo->Text = GetNonNull(JRespObj, "CustomerNo", "");
		// Enable the BookOrder button
		btnBookOrder->Enabled = true;
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("Fill Fields Exception:" + E.Message);
		return;
		}

	try
		{
		// Check to see if there is an outstanding trip (IsDuplicate)
		if (JRespObj->GetValue("IsDuplicate")->Value() == "true")
			{
			//
			// Get the FareInfo since its a dup
			//
			TJSONObject *FareObj = (TJSONObject*)JRespObj->Get("FareInfo")->JsonValue;
			edtTSTripID->Text = GetNonNull(FareObj, "TripID", " ");
			lblTSStatus->Text = GetNonNull(FareObj, "TripStatus", " ");
			lblTSVehicleNo->Text = GetNonNull(FareObj, "VehicleNo", " ");
			btnGCCancel->Enabled = true;
			btnGCCancelTrip->Enabled = true;
			lblTripIDStatus->Text = edtTSTripID->Text + " : " + lblTSStatus->Text + " Cab#: " + lblTSVehicleNo->Text;
			lblDistanceETA->Text = "Distance: " + GetNonNull(FareObj, "Distance", "0.0") + " " +
				 GetNonNull(FareObj, "DistanceUnit", "M") + " ETA: " + GetNonNull(FareObj, "ETA", "-1"); ;
			lblTripIDStatus->Enabled = true;
			lblTripIDStatus->Visible = true;
			}
		else
			{
			edtTSTripID->Text = "";
			lblTSStatus->Text = "";
			lblTSVehicleNo->Text = "";
			btnGCCancel->Enabled = false;
			btnGCCancelTrip->Enabled = false;
			lblTripIDStatus->Enabled = false;
			lblTripIDStatus->Visible = false;
			lblDistanceETA->Text = "";
			}
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("TripStatus Exception:" + E.Message);
		}

	//
	// Fill in the Pickup Info from Response
	//
	try
		{
		oAddr = (TJSONObject*)JRespObj->Get("Pickup")->JsonValue;
		edtGCPAccessCode->Text = GetNonNull(oAddr, "AccessCode", " ");
		edtGCPAddressID->Text = GetNonNull(oAddr, "AddressID", " ");
		edtGCPApartment->Text = GetNonNull(oAddr, "Apartment", " ");
		edtGCPBuilding->Text = GetNonNull(oAddr, "BuildingName", " ");
		edtGCPCity->Text = GetNonNull(oAddr, "CityName", " ");
		edtGCPLatitude->Text = GetNonNull(oAddr, "Latitude", "0.0");
		edtGCPLongitude->Text = GetNonNull(oAddr, "Longitude", "0.0");
		edtGCPName->Text = GetNonNull(oAddr, "Name", " ");
		edtGCPStreet->Text = GetNonNull(oAddr, "StreetName", " ");
		edtGCPStreetNo->Text = GetNonNull(oAddr, "StreetNumber", " ");
		edtGCPZip->Text = GetNonNull(oAddr, "ZipCode", " ");
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("Pickup Address Exception:" + E.Message);
		}
	//
	// Fill in the Dropoff
	//
	try
		{
		oAddr = (TJSONObject*)JRespObj->Get("Dropoff")->JsonValue;
		edtGCDAccessCode->Text = GetNonNull(oAddr, "AccessCode", " ");
		edtGCDAddressID->Text = GetNonNull(oAddr, "AddressID", " ");
		edtGCDApartment->Text = GetNonNull(oAddr, "Apartment", " ");
		edtGCDBuilding->Text = GetNonNull(oAddr, "BuildingName", " ");
		edtGCDCity->Text = GetNonNull(oAddr, "CityName", " ");
		edtGCDLatitude->Text = GetNonNull(oAddr, "Latitude", "0.0");
		edtGCDLongitude->Text = GetNonNull(oAddr, "Longitude", "0.0");
		edtGCDName->Text = GetNonNull(oAddr, "Name", " ");
		edtGCDStreet->Text = GetNonNull(oAddr, "StreetName", " ");
		edtGCDStreetNo->Text = GetNonNull(oAddr, "StreetNumber", " ");
		edtGCDZip->Text = GetNonNull(oAddr, "ZipCode", " ");
		}
	catch (Exception &E)
		{
		memDebug->Lines->Add("Dropoff Address Exception:" + E.Message);
		}
	//
	// Addresses
	//

	// Get List of addresses from Response
	TJSONArray *AddressList;
	try
		{
		AddressList = (TJSONArray*)JRespObj->Get("Addresses")->JsonValue;
		// Load the ComboBox and Address Components for the first item in list
		try
			{
			for (int ii = 0; ii < AddressList->Count; ii++)
				{
				oAddr = (TJSONObject*)AddressList->Items[ii];
				// Add to ComboBox
				// cbxAddresses->Items->Add(oAddr->GetValue("StreetNumber")->Value().Trim() + oAddr->GetValue("StreetName")->Value()
				// .Trim());
				cbxAddresses->Items->AddObject(oAddr->GetValue("StreetNumber")->Value().Trim() + " " +
					 oAddr->GetValue("StreetName")->Value().Trim(), AddressList->Items[ii]);
				cbxBOAddresses->Items->AddObject(oAddr->GetValue("StreetNumber")->Value().Trim() + " " +
					 oAddr->GetValue("StreetName")->Value().Trim(), AddressList->Items[ii]);
				}
			// Set the ComboBox to the first item.
			// Doing this will trigger the OnChange Event handler that will load the Components
			if (cbxAddresses->Items->Count > 0)
				cbxAddresses->ItemIndex = 0;
			if (cbxBOAddresses->Items->Count > 0)
				cbxBOAddresses->ItemIndex = 0;
			}
		catch (Exception &E)
			{

			}
		}
	catch (Exception &E)
		{
		// AddressList not valid
		}

	//
	// Misc
	//
	// Remarks
	TJSONArray *RemarksList;
	cbxMRemarks->Items->Clear();
	try
		{
		// RemarksList = (TJSONArray*)JRespObj->Get("Remarks")->JsonValue;
		RemarksList = (TJSONArray*)JRespObj->GetValue("Remarks");

		for (int ii = 0; ii < RemarksList->Count; ii++)
			{
			oAddr = (TJSONObject*)RemarksList->Items[ii];
			// Add to ComboBox
			cbxMRemarks->Items->Add("Line " + IntToStr(ii + 1) + ". " + oAddr->GetValue("Remark")->Value().Trim());
			}

		if (cbxMRemarks->Items->Count > 0)
			cbxMRemarks->ItemIndex = 0;
		}
	catch (Exception &E)
		{

		}

	// IsDuplicate
	lblMIsDuplicate->Text = GetNonNull(JRespObj, "IsDuplicate", "false");
	// IsBlocked
	lblMIsBlocked->Text = GetNonNull(JRespObj, "IsBlocked", "false");
	// RebuiltTime
	lblMRebuiltTime->Text = GetNonNull(JRespObj, "RebuiltTime", "");
	// LastUpdate
	lblMLastUpdate->Text = GetNonNull(JRespObj, "LastUpdate", "");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::lbSimulatorClick(TObject *Sender)
	{
	TabControl1->ActiveTab = tabSimulator;
	lbSimulator->TagObject = tabSimulator;
	MultiView1->HideMaster();
	MultiView1->Enabled = false;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::btnGCCancelClick(TObject *Sender)
	{
	TJSONObject *JCancelBody = new TJSONObject();
	UnicodeString BaseURL = "HTTP://";
	// Cancel Trip
	// Build the BaseURL
	BaseURL += edtHost->Text + ":5005/IVRMgr";
	BaseURL += "?Command=CancelOrder";
	BaseURL += "&CalledID=" + edtGCCalledID->Text.Trim();
	BaseURL += "&CallerID=" + edtGCCallerID->Text.Trim();
	BaseURL += "&UniqueID=" + edtGCUniqueID->Text.Trim();
	dmRestClient->IVRClient->BaseURL = BaseURL;
	// Set the Parameter: Body { "FareNumber":"1234"}
	// dmRestClient->IVRCancelRequest->Body->
	// Set the Parameters
	JCancelBody->AddPair("FareNumber", edtTSTripID->Text.Trim());
	dmRestClient->IVRCancelRequest->Params->ParameterByName("body")->Value = JCancelBody->ToJSON();
	//
	// Execute the Request
	//
	try
		{
		dmRestClient->IVRCancelRequest->Execute();
		}
	catch (Exception &E)
		{
		lblGCStatus->Text = E.Message;
		return;
		}

	//
	// Check for Success in REST
	//
	if (dmRestClient->IVRCancelRequest->Response->Status.Success())
		{
		lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRCancelRequest->Response->StatusCode) + ") ";
		lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRCancelRequest->Response->StatusText;
		}
	else
		{
		lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRCancelRequest->Response->StatusCode) + ") ";
		lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRCancelRequest->Response->StatusText;
		return;
		}

	// Request was successful show the JSON Response
	memGC->Lines->Add(dmRestClient->IVRCancelResponse->JSONText);

	// Get a JSON Object from the Response so we can access the elements
	TJSONObject *JRespObj = (TJSONObject*) TJSONObject::ParseJSONValue
		 (TEncoding::ASCII->GetBytes(dmRestClient->IVRCancelResponse->JSONText), 0);
	//
	// Info data for all responses
	//
	lblGCICommand->Text = JRespObj->GetValue("Command")->Value();
	lblGCIActionID->Text = JRespObj->GetValue("ActionID")->Value();
	lblGCIResult->Text = JRespObj->GetValue("Result")->Value();
	//
	// Note: Cabmate is not returning ResultCode
	//
	try
		{
		lblGCIResultCode->Text = JRespObj->GetValue("ResultCode")->Value();
		}
	catch (Exception &E)
		{
		lblGCIResultCode->Text = "";
		}

	// TODO: Response is returned by Cabmate but not XDS

	lblGCIErrorMsg->Text = JRespObj->GetValue("ErrorMsg")->Value();
	lblGCStatus->Text = lblGCStatus->Text + lblGCIErrorMsg->Text;

	//
	// Error Response from REST Service (Cancel Failed)
	//
	if (JRespObj->GetValue("Result")->Value() != "Success")
		{
		lblGCStatus->Text = lblGCStatus->Text + " JSON Response:" + JRespObj->GetValue("ErrorMsg")->Value();
		lblGCIVehicleNo->Text = "";
		lblGCIAccountNo->Text = "";
		lblGCIAccountName->Text = "";
		lblGCICustomerNo->Text = "";
		lblGCICellPhone->Text = "";
		lblGCITripID->Text = "";
		lblGCIDispatchUniqueID->Text = "";
		lblGCIFleet->Text = "";
		}
	else
		{
		btnGCCancelTrip->Enabled = false;
		lblTripIDStatus->Text = "";
		lblTripIDStatus->Enabled = false;
		lblTripIDStatus->Visible = false;
		lblDistanceETA->Text = "";
		}

	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::edtTSTripIDKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
	 TShiftState Shift)
	{
	if (edtTSTripID->Text.Trim() != "")
		btnGCCancel->Enabled = true;
	else
		btnGCCancel->Enabled = false;
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::cbxAddressesChange(TObject *Sender)
	{
	// Set the Components based on the new ItemIndex
	if (cbxAddresses->Items->Count > 0)
		{
		TJSONObject *oAddr = (TJSONObject*)cbxAddresses->Items->Objects[cbxAddresses->ItemIndex];
		lblADAddressIndex->Text = GetNonNull(oAddr, "AddressIndex", "");
		lblADName->Text = GetNonNull(oAddr, "Name", "");
		lblADLatitude->Text = GetNonNull(oAddr, "Latitude", "");
		lblADLongitude->Text = GetNonNull(oAddr, "Longitude", "");
		lblADDispatchZone->Text = GetNonNull(oAddr, "DispatchZone", "");
		lblADMapNumber->Text = GetNonNull(oAddr, "MapNumber", "");
		lblADMapCoordinates->Text = GetNonNull(oAddr, "MapCoordinates", "");
		lblADZipCode->Text = GetNonNull(oAddr, "ZipCode", "");
		lblADStreetNumber->Text = GetNonNull(oAddr, "StreetNumber", "");
		lblADStreetName->Text = GetNonNull(oAddr, "StreetName", "");
		lblADCityName->Text = GetNonNull(oAddr, "CityName", "");
		lblADBuildingName->Text = GetNonNull(oAddr, "BuildingName", "");
		lblADApartment->Text = GetNonNull(oAddr, "Apartment", "");
		lblADAccessCode->Text = GetNonNull(oAddr, "AccessCode", "");
		}
	}

UnicodeString __fastcall TfrmRestClient::GetNonNull(TJSONObject *oJObj, UnicodeString FldName, UnicodeString DefVal)
	{
	UnicodeString uRet;
	try
		{
		uRet = oJObj->GetValue(FldName)->Value();
		}
	catch (Exception &E)
		{
		uRet = DefVal;
		}
	return (uRet);
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnClearOutputClick(TObject *Sender)
	{
	memGC->Lines->Clear();
	lblPackets->Text = "0";
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::cbxBOAddressesChange(TObject *Sender)
	{
	// Set the Components based on the new ItemIndex
	if (cbxBOAddresses->Items->Count > 0)
		{
		TJSONObject *oAddr = (TJSONObject*)cbxBOAddresses->Items->Objects[cbxBOAddresses->ItemIndex];
		lblBOName->Text = GetNonNull(oAddr, "Name", "");
		lblBOLatitude->Text = GetNonNull(oAddr, "Latitude", "");
		lblBOLongitude->Text = GetNonNull(oAddr, "Longitude", "");
		lblBODispatchZone->Text = GetNonNull(oAddr, "DispatchZone", "");
		lblBOMapNo->Text = GetNonNull(oAddr, "MapNumber", "");
		lblBOMapCoords->Text = GetNonNull(oAddr, "MapCoordinates", "");
		lblBOZipCode->Text = GetNonNull(oAddr, "ZipCode", "");
		lblBOStreetNo->Text = GetNonNull(oAddr, "StreetNumber", "");
		lblBOStreetName->Text = GetNonNull(oAddr, "StreetName", "");
		lblBOCityName->Text = GetNonNull(oAddr, "CityName", "");
		lblBOBuildingName->Text = GetNonNull(oAddr, "BuildingName", "");
		lblBOApartment->Text = GetNonNull(oAddr, "Apartment", "");
		lblBOAccessCode->Text = GetNonNull(oAddr, "AccessCode", "");
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::chkImmediateChange(TObject *Sender)
	{
	if (chkImmediate->IsChecked)
		{
		teTripTime->UseNowTime = true;
		deTripTime->TodayDefault = true;
		}
	else
		{
		teTripTime->UseNowTime = false;
		deTripTime->TodayDefault = false;
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnBookOrderClick(TObject *Sender)
	{
	TTime StartTime;
	// Build the REST request
	TJSONObject *JBookBody = new TJSONObject();
	TJSONObject *JPickup = new TJSONObject();
	TJSONObject *JDropoff = new TJSONObject();
	TJSONArray *JRemarks = new TJSONArray();
	TJSONObject *JRemark = new TJSONObject();
	TJSONObject *JObj = new TJSONObject();
	UnicodeString tmpStr = "";

	UnicodeString BaseURL = "HTTP://";
	// Book the order
	// Build the BaseURL
	BaseURL += edtHost->Text + ":5005/IVRMgr?Command=BookOrder";
	BaseURL += "&CalledID=" + edtGCCalledID->Text.Trim();
	// BaseURL += "&CallerID=" + edtGCCallerID->Text.Trim();
	// BaseURL += "&UniqueID=" + edtGCUniqueID->Text.Trim();
	dmRestClient->IVRClient->BaseURL = BaseURL;
	// dmRestClient->IVRBookRequest->Params->Clear();
	// dmRestClient->IVRBookRequest->Params->ParameterByName("CalledID")->Value = edtGCCalledID->Text;
	// dmRestClient->IVRBookRequest->Params->ParameterByName("CallerID")->Value = edtGCCallerID->Text;
	// dmRestClient->IVRBookRequest->Params->ParameterByName("UniqueID")->Value = edtGCUniqueID->Text;

	// Set the Parameters in JSON Booking Request
	JBookBody->AddPair("CalledID", edtGCCalledID->Text.Trim());
	JBookBody->AddPair("CallerID", edtGCCallerID->Text.Trim());
	JBookBody->AddPair("UniqueID", edtGCUniqueID->Text.Trim());
	JBookBody->AddPair("Phone", lblBOPhoneNo->Text.Trim());
	JBookBody->AddPair("VehicleNo", lblGCIVehicleNo->Text.Trim());
	JBookBody->AddPair("AccountNo", lblBOAccountNo->Text.Trim());
	JBookBody->AddPair("AccountName", lblBOAccountName->Text.Trim());
	JBookBody->AddPair("CustomerNo", lblBOCustomerNo->Text.Trim());
	JBookBody->AddPair("CellPhone", lblBOPhoneNo->Text.Trim());
	JBookBody->AddPair("TripID", lblGCITripID->Text.Trim());
	JBookBody->AddPair("DispatchUniqueID", lblGCIDispatchUniqueID->Text.Trim());
	JBookBody->AddPair("Fleet", lblBOFleet->Text.Trim());
	// turn off the Cancel Button
	btnBOCancel->Enabled = true;
	// Immediate or Future fare
	if (chkImmediate->IsChecked)
		{
		JBookBody->AddPair("ImmediateOrFuture", "I");
		Log("Now String:" + System::Dateutils::DateToISO8601(Now(), true));
		JBookBody->AddPair("TripTime", System::Dateutils::DateToISO8601(Now(), true));
		}
	else
		{
		JBookBody->AddPair("ImmediateOrFuture", "F");
		JBookBody->AddPair("TripTime", "TODO");
		}
	// Payment Type
	if (radBOCash->IsChecked)
		JBookBody->AddPair("PaymentType", "CASH");
	if (radBOCredit->IsChecked)
		JBookBody->AddPair("PaymentType", "CREDIT");
	if (radBOAccount->IsChecked)
		JBookBody->AddPair("PaymentType", "ACCOUNT");

	JBookBody->AddPair("Passengers", cbxPassengers->Selected->Text);
	// Remarks
	/*
	 JRemark->AddPair("Remark","Remark 1 from CPP");
	 JRemark->AddPair("Remark","Remark 2 from CPP");
	 JRemark->AddPair("Remark","Remark 3 from CPP");
	 JRemark->AddPair("Remark","Remark 4 from CPP");
	 JRemarks->AddElement(JRemark);
	 // didnt work -- added all as "TRUE"
	 JRemarks->Add("Remark 1from CPP");
	 JRemarks->Add("Remark 2 from CPP");
	 JRemarks->Add("Remark 3 from CPP");
	 JRemarks->Add("Remark 4 from CPP");
	 */

	tmpStr = "[";
	tmpStr += "\"Remark1\",";
	tmpStr += "\"Remark2\",";
	tmpStr += "\"Remark3\"]";
	try
		{
		JObj = (TJSONObject*)TJSONObject::ParseJSONValue(tmpStr);
		}
	catch (Exception &E)
		{
		tmpStr = E.Message;
		}

	// JBookBody->
	JBookBody->AddPair("Remarks", JObj);
	tmpStr = JBookBody->ToString();

	// Pickup
	JPickup->AddPair("Name", lblBOName->Text.Trim());
	JPickup->AddPair("AddressID", "0");
	JPickup->AddPair("Latitude", lblBOLatitude->Text.Trim());
	JPickup->AddPair("Longitude", lblBOLongitude->Text.Trim());
	JPickup->AddPair("DispatchZone", "0");
	JPickup->AddPair("MapNumber", lblBOMapNo->Text.Trim());
	JPickup->AddPair("MapCoordinates", lblBOMapCoords->Text.Trim());
	JPickup->AddPair("ZipCode", lblBOZipCode->Text.Trim());
	JPickup->AddPair("StreetNumber", lblBOStreetNo->Text.Trim());
	JPickup->AddPair("StreetName", lblBOStreetName->Text.Trim());
	JPickup->AddPair("CityName", lblBOCityName->Text.Trim());
	JPickup->AddPair("BuildingName", lblBOBuildingName->Text.Trim());
	JPickup->AddPair("Apartment", lblBOApartment->Text.Trim());
	JPickup->AddPair("AccessCode", lblBOAccessCode->Text.Trim());
	JPickup->AddPair("Passengers", cbxPassengers->Selected->Text);
	// Payment Type
	if (radBOCash->IsChecked)
		JPickup->AddPair("PaymentMethod", "CASH");
	if (radBOCredit->IsChecked)
		JPickup->AddPair("PaymentMethod", "CREDIT");
	if (radBOAccount->IsChecked)
		JPickup->AddPair("PaymentMethod", "ACCOUNT");

	JPickup->AddPair("Fleet", lblBOFleet->Text.Trim());
	JBookBody->AddPair("Pickup", JPickup);
	// Dropoff
	// TODO:
	JDropoff->AddPair("Name", "");
	JDropoff->AddPair("AddressID", "0");
	JDropoff->AddPair("Latitude", "0");
	JDropoff->AddPair("Longitude", "0");
	JDropoff->AddPair("DispatchZone", "0");
	JDropoff->AddPair("MapNumber", "0");
	JDropoff->AddPair("MapCoordinates", "");
	JDropoff->AddPair("ZipCode", "");
	JDropoff->AddPair("StreetNumber", "");
	JDropoff->AddPair("StreetName", "");
	JDropoff->AddPair("CityName", "");
	JDropoff->AddPair("BuildingName", "");
	JDropoff->AddPair("Apartment", "");
	JDropoff->AddPair("AccessCode", "");
	JBookBody->AddPair("Dropoff", JDropoff);

	dmRestClient->IVRBookRequest->Params->ParameterByName("body")->Value = JBookBody->ToJSON();

	memGC->Lines->Add("Booking Request:");
	memGC->Lines->Add(dmRestClient->IVRBookRequest->FullResource);
	memGC->Lines->Add(dmRestClient->IVRBookRequest->Params->ParameterByName("body")->Value);

	memGC->Lines->Add(dmRestClient->IVRBookRequest->GetFullRequestURL(true));

	//
	// Execute the Request
	//
	StartTime = Time();
	try
		{
		dmRestClient->IVRBookRequest->Execute();
		lblBIResponseTime->Text = FloatToStr(MilliSecondsBetween(StartTime, Time())) + " ms";
		lblBIContentLength->Text = IntToStr((int)dmRestClient->IVRBookResponse->ContentLength) + " bytes";
		}
	catch (Exception &E)
		{
		lblGCStatus->Text = E.Message;
		lblBIResponseTime->Text = FloatToStr(MilliSecondsBetween(StartTime, Time())) + " ms";
		return;
		}

	//
	// Check for Success in REST
	//
	if (dmRestClient->IVRBookRequest->Response->Status.Success())
		{
		lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRBookRequest->Response->StatusCode) + ") ";
		lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRBookRequest->Response->StatusText;
		}
	else
		{
		lblGCStatus->Text = "Response (" + IntToStr(dmRestClient->IVRBookRequest->Response->StatusCode) + ") ";
		lblGCStatus->Text = lblGCStatus->Text + ":" + dmRestClient->IVRBookRequest->Response->StatusText;
		return;
		}

	// Request was successful show the JSON Response
	memGC->Lines->Add(dmRestClient->IVRBookResponse->JSONText);

	// Get a JSON Object from the Response so we can access the elements
	TJSONObject *JRespObj = (TJSONObject*) TJSONObject::ParseJSONValue
		 (TEncoding::ASCII->GetBytes(dmRestClient->IVRBookResponse->JSONText), 0);
	//
	// Info data for all responses
	//
	lblBITimeStamp->Text = GetNonNull(JRespObj, "TimeStamp", "");
	lblBICommand->Text = GetNonNull(JRespObj, "Command", "");
	lblBIActionID->Text = GetNonNull(JRespObj, "ActionID", "");
	lblBIResult->Text = GetNonNull(JRespObj, "Result", "");
	lblBIResultCode->Text = GetNonNull(JRespObj, "ResultCode", "");
	lblBIResponse->Text = GetNonNull(JRespObj, "Response", "");
	lblBIErrorMsg->Text = GetNonNull(JRespObj, "ErrorMsg", "");

	//
	// Successful booking
	//
	if (GetNonNull(JRespObj, "Result", "") == "Success")
		{
		btnBOCancel->Enabled = true;
		lblGCStatus->Text = lblGCStatus->Text + " Job Number:" + GetNonNull(JRespObj, "JobNumber", "");
		}
	// These will work even if it failed since it clears the value of it is not found
	// IE: Booking failed so there is No JobNo in response so it will be blanked out
	lblBIJobNo->Text = GetNonNull(JRespObj, "JobNumber", "");
	lblBIDispatchZone->Text = GetNonNull(JRespObj, "DispatchZone", "");
	lblBIFareType->Text = GetNonNull(JRespObj, "FareType", "");
	// Set the TripStatus TripID so we can just that Cancel procedure
	edtTSTripID->Text = lblBIJobNo->Text;
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::btnGCTimerClick(TObject *Sender)
	{
	if (tmrGetCust->Enabled)
		tmrGetCust->Enabled = false;
	else
		tmrGetCust->Enabled = true;

	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::nbxGCTimerChange(TObject *Sender)
	{
	tmrGetCust->Interval = (int)((int)nbxGCTimer->Value * 1000);
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::memDebugDblClick(TObject *Sender)
	{
	memDebug->Lines->Clear();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::memGCChange(TObject *Sender)
	{
	lblMemGCLines->Text = IntToStr(memGC->Lines->Count);
	if (memGC->Lines->Count > 5000)
		{
		memGC->Lines->Clear();
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerConnect(TIdContext *AContext)
	{
	GCLog("TCPListenerConnect");
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerDisconnect(TIdContext *AContext)
	{
	GCLog("TCPListenerDisconnect:");
	AContext->Connection->Disconnect();
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerException(TIdContext *AContext, Exception *AException)
	{
	GCLog("TCPListenerException:" + AException->Message);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerListenException(TIdListenerThread *AThread, Exception *AException)
	{
	GCLog("TCPListenerListenException:" + AException->Message);
	GCLog(AException->StackTrace);
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerStatus(TObject *ASender, const TIdStatus AStatus,
	 const UnicodeString AStatusText)
	{
	GCLog("TCPListenerStatus:" + AStatusText);
	}

void __fastcall TfrmRestClient::OnDisconnect(TObject *Sender)
	{
	GCLog("OnDisconnect");
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnListenClick(TObject *Sender)
	{
	TCPListener->Active =  !TCPListener->Active;
	if (TCPListener->Active)
		{
		GCLog("TCPListener->Active = true");
		btnListen->StaysPressed = true;
		}
	else
		{
		GCLog("TCPListener->Active = false");
		btnListen->StaysPressed = false;
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::TCPListenerExecute(TIdContext *AContext)
	{
	int ii = 0;
	int MessageLength = 0;
	bool bStillMore = true;
	// Set this if we need to debug the DePickler
	// LogDePickler->memOut = memGC;

	// GCLog("::TCPListenerExecute ReceiveBufferSize:" + IntToStr(AContext->Connection->Socket->RecvBufferSize));
	AContext->Connection->OnDisconnected = OnDisconnect;
	// Trying to read after socket is connected
	try
		{
		// GCLog("Before Loop Get MessageLength");
		MessageLength = AContext->Connection->IOHandler->ReadInt32();
		}
	catch (Exception &E)
		{
		// GCLog("ReadInt32(): Exception:" + E.Message);
		bStillMore = false;
		}

	while (bStillMore)
		{
		lblPackets->Text = IntToStr(StrToInt(lblPackets->Text) + 1);
		// GCLog("DePickling message");
		LogDePickler->DePickle(MessageLength, AContext->Connection->IOHandler);
		try
			{
			// GCLog("msg.Length:" + IntToStr(LogDePickler->Fields["msg"].Length()));
			// One version of Python it comes in as msg, the other is message
			if ( LogDePickler->Fields["msg"].Length() > LogDePickler->Fields["message"].Length())
				GCLog(LogDePickler->Fields["asctime"] + " : " + LogDePickler->Fields["msg"]);
			else
				GCLog(LogDePickler->Fields["asctime"] + " : " + LogDePickler->Fields["message"]);
			// memGC->Repaint();
			// Application->HandleMessage();

			memGC->GoToTextEnd();
			memGC->ApplyStyleLookup();
			this->Invalidate();
			Application->ProcessMessages();
			}
		catch (Exception &E)
			{
			GCLog("Exception Adding Pickler Data:" + E.Message);
			}

		// for(std::map<UnicodeString,UnicodeString>::const_iterator Iter = LogDePickler->Fields.begin(); Iter !=  LogDePickler->Fields.end();Iter++)
		// memGC->Lines->Add("Key: " + Iter->first + " Value:" + Iter->second);

		// memGC->Lines->Add(LogDePickler->DebugString);
		try
			{
			// GCLog("Inside Loop Get MessageLength");

			MessageLength = AContext->Connection->IOHandler->ReadInt32();
			// GCLog("Inside Loop MessageLength = " + IntToStr(MessageLength));
			}
		catch (Exception &E)
			{
			// GCLog("ReadInt32(): Exception:" + E.Message);
			bStillMore = false;
			}
		}
	}
// ---------------------------------------------------------------------------

void __fastcall TfrmRestClient::IdServerInterceptLogEvent1LogString(TIdServerInterceptLogEvent *ASender,
	 const UnicodeString AText)
	{
	// Log("Intercept:" + AText);
	}

void __fastcall TfrmRestClient::GoogleLog(UnicodeString asLog)
	{
	asLog = GetCurrentDateTimeMS(L"hh:NN:ss") + ":" + asLog;

	if (memGoogle != NULL)
		{
		memGoogle->Lines->Add(asLog);
		memGoogle->Repaint();
		Application->ProcessMessages();
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnGeocodeClick(TObject *Sender)
	{
	TRESTRequest *Req = dmRestClient->GoogleGeocodeRequest;
	TRESTResponse *Res = dmRestClient->GoogleGeocodeResponse;
	TJSONObject *JObjRoot;
	TJSONObject *JObj;
	TJSONObject *JElement;
	TJSONArray *JResults;
	TJSONArray *JArray;

	// Geocode
	// setup the address as a geocode
	Req->Params->ParameterByName("key")->Value = edtGoogleAPIKey->Text.Trim();
	Req->Params->ParameterByName("address")->Value = edtGeoAddress->Text.Trim();
	try
		{
		Req->Execute();
		}
	catch (Exception &E)
		{
		GoogleLog("Execute Exception:" + E.Message);
		return;
		}

	GoogleLog(Res->JSONText);

	// Get a JSON Object from the Response so we can access the elements
	JObjRoot = (TJSONObject*) TJSONObject::ParseJSONValue(TEncoding::ASCII->GetBytes(Res->JSONText), 0);

	GoogleLog("status:" + GetNonNull(JObjRoot, "status", "Unknown"));

	if (GetNonNull(JObjRoot, "status", "Unknown") != "OK")
		return;

	// get the array of results - THIS WORKS
	JArray = (TJSONArray*)JObjRoot->Get("results")->JsonValue;
	GoogleLog("results array count:" + IntToStr(JArray->Count) + " Should be 1");
	// Get the array of items in Results - THIS WORKS
	JResults = (TJSONArray*)JArray->Items[0];
	GoogleLog("results array count:" + IntToStr(JResults->Count) + " Should be 5");
	try
		{
		JElement = (TJSONObject*)JArray->Items[0];
		JObj = (TJSONObject*)JElement->Get("address_components")->JsonValue;
		GoogleLog(JObj->ToString());
		}
	catch (Exception &E)
		{
		GoogleLog("Exception:" + E.Message);
		return;
		}

	//
	// Address Components
	//
	try
		{
		TJSONArray *aAddr = (TJSONArray*)JElement->Get("address_components")->JsonValue;
		GoogleLog("Address Components Count:" + IntToStr(aAddr->Count));
		for (int ii = 0; ii < aAddr->Count; ii++)
			{
			JElement = (TJSONObject*)aAddr->Items[ii];
			JObj = (TJSONObject*)JElement->Get("long_name")->JsonValue;
			GoogleLog("long_name:" + JObj->ToString());
			JObj = (TJSONObject*)JElement->Get("short_name")->JsonValue;
			GoogleLog("short_name:" + JObj->ToString());
			TJSONArray *aTypes = (TJSONArray*)JElement->Get("types")->JsonValue;
			GoogleLog("-- Type Count:" + IntToStr(aTypes->Count));
			for (int jj = 0; jj < aTypes->Count; jj++)
				{
				JElement = (TJSONObject*)aTypes->Items[jj];
				GoogleLog("-- Type:" + JElement->ToString());
				}
			}
		}
	catch (Exception &E)
		{
		GoogleLog("Exception:" + E.Message);
		return;
		}

	//
	// formatted_address
	//
	try
		{
		JElement = (TJSONObject*)JArray->Items[0];
		JObj = (TJSONObject*)JElement->Get("formatted_address")->JsonValue;
		GoogleLog("formatted_address:" + JObj->Value());
		}
	catch (Exception &E)
		{
		}

	//
	// place_id
	//
	try
		{
		JElement = (TJSONObject*)JArray->Items[0];
		JObj = (TJSONObject*)JElement->Get("place_id")->JsonValue;
		GoogleLog("place_id:" + JObj->Value());
		}
	catch (Exception &E)
		{
		}

	//
	// geometry
	//
	try
		{
		JElement = (TJSONObject*)JArray->Items[0];
		JObj = (TJSONObject*)JElement->Get("geometry")->JsonValue;
		GoogleLog("geometry:" + JObj->Value());
		TJSONObject *oLoc = (TJSONObject*)JObj->Get("location")->JsonValue;
		GoogleLog("location:" + oLoc->Value());
		JObj = (TJSONObject*)oLoc->Get("lat")->JsonValue;
		GoogleLog("lat:" + JObj->Value());
		JObj = (TJSONObject*)oLoc->Get("lng")->JsonValue;
		GoogleLog("lng:" + JObj->Value());

		}
	catch (Exception &E)
		{

		}

	//
	// types
	//
	try
		{
		JElement = (TJSONObject*)JArray->Items[0];
		TJSONArray *aTypes = (TJSONArray*)JElement->Get("types")->JsonValue;
		GoogleLog("-- Type Count:" + IntToStr(aTypes->Count));
		for (int jj = 0; jj < aTypes->Count; jj++)
			{
			JElement = (TJSONObject*)aTypes->Items[jj];
			GoogleLog("-- Type:" + JElement->ToString());
			}
		}
	catch (Exception &E)
		{
		}

	}

// ---------------------------------------------------------------------------
void __fastcall TfrmRestClient::btnClearGoogleClick(TObject *Sender)
	{
	memGoogle->Lines->Clear();
	}
// ---------------------------------------------------------------------------
