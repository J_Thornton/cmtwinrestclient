// ---------------------------------------------------------------------------

#ifndef CMTWinRestClientFormH
#define CMTWinRestClientFormH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Gestures.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Edit.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Ani.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.MultiView.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <IPPeerClient.hpp>
#include <REST.Client.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.Grid.hpp>
#include "FMX.TMSBaseControl.hpp"
// #include "FMX.TMSCustomGrid.hpp"
#include "FMX.TMSGrid.hpp"
#include "FMX.TMSGridCell.hpp"
#include "FMX.TMSGridData.hpp"
#include "FMX.TMSGridOptions.hpp"
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.ExtCtrls.hpp>
#include "FMX.TMSCustomTreeView.hpp"
#include "FMX.TMSTreeView.hpp"
#include "FMX.TMSTreeViewBase.hpp"
#include "FMX.TMSTreeViewData.hpp"
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdGlobal.hpp>
#include <IdIntercept.hpp>
#include <IdIOHandler.hpp>
#include <IdIOHandlerSocket.hpp>
#include <IdIOHandlerStack.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include "FMX.TMSLed.hpp"
#include <FMX.Objects.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include "FMX.TMSCheckGroupPicker.hpp"
#include "FMX.TMSCustomPicker.hpp"
#include <FMX.ComboEdit.hpp>
#include "AsteriskTools.h"
#include <FMX.DateTimeCtrls.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.SpinBox.hpp>
#include <IdCustomTCPServer.hpp>
#include <IdTCPServer.hpp>
#include <Datasnap.DSCommonServer.hpp>
#include <Datasnap.DSTCPServerTransport.hpp>
#include <IPPeerServer.hpp>
#include <IdContext.hpp>
#include <IdServerIOHandler.hpp>
#include <IdServerIOHandlerSocket.hpp>
#include <IdServerIOHandlerStack.hpp>
#include <System.SysUtils.hpp>
#include <IdServerInterceptLogBase.hpp>
#include <IdServerInterceptLogEvent.hpp>
#include "FMX.TMSCustomGrid.hpp"
#ifndef TARGET_OS_MAC
#include "sgcWebSocket.hpp"
#include "sgcWebSocket_Classes.hpp"
#include "sgcWebSocket_Classes_Indy.hpp"
#include "sgcWebSocket_Client.hpp"
#endif

// ---------------------------------------------------------------------------
class TfrmRestClient : public TForm
	{
__published: // IDE-managed Components
	TToolBar * HeaderToolBar;
	TLabel * ToolBarLabel;
	TTabControl * TabControl1;
	TTabItem * tabP2D;
	TTabItem * tabLog;
	TTabItem * tabExtensions;
	TTabItem * tabSettings;
	TToolBar * ToolBar1;
	TButton * Button2;
	TLayout * Layout1;
	TLayout * Layout2;
	TLayout * Layout3;
	TLabel * lblP2DResponse;
	TLayout * layoutPassengerPhone;
	TBindingsList * BindingsList1;
	TEdit * edtDriverPhone;
	TLinkControlToField * LinkControlToField3;
	TLabel * lblDriverPhone;
	TLayout * layoutDriverPhone;
	TLayout * lytDriverPhone;
	TLayout * lytDriverPhoneEdit;
	TLayout * lytPassengerPhone;
	TLayout * lytPassengerPhoneEdit;
	TLabel * lblPassengerPhone;
	TEdit * edtPassengerPhone;
	TLinkControlToField * LinkControlToField4;
	TLayout * lytCallerID;
	TLayout * lytCallerIDEdit;
	TEdit * edtCallerID;
	TLinkControlToField * LinkControlToField5;
	TLabel * lblCallerID;
	TLinkPropertyToField * LinkPropertyToFieldText;
	TTabControl * TabControl2;
	TTabItem * TabItem5;
	TTabItem * TabItem6;
	TTabItem * TabItem7;
	TMemo * Memo2;
	TLinkControlToField * LinkControlToField2;
	TMemo * Memo3;
	TLinkControlToField * LinkControlToField6;
	TStyleBook * StyleBook1;
	TColorAnimation * ColorAnimation1;
	TLayout * Layout16;
	TMemo * Memo1;
	TLinkControlToField * LinkControlToField1;
	TClearEditButton * clrCallerID;
	TClearEditButton * clrDriverPhone;
	TClearEditButton * clrPassengerPhone;
	TLayout * layoutCallerID;
	TMultiView * MultiView1;
	TListBox * ListBox1;
	TLayout * Layout4;
	TLayout * Layout7;
	TLayout * Layout8;
	TLabel * Label1;
	TLayout * Layout9;
	TEdit * edtExtFilter;
	TClearEditButton * ClearEditButton2;
	TButton * btnRetrieve;
	TStringGrid * grdExtensions;
	TLinkControlToField * LinkControlToField9;
	TLayout * Layout11;
	TLayout * Layout5;
	TLabel * lblCategory;
	TLayout * Layout6;
	TEdit * edtExtCategory;
	TClearEditButton * clrCategory;
	TLayout * Layout12;
	TLayout * layoutBaseURL;
	TLayout * lytBaseURL;
	TLabel * lblBaseURL;
	TLayout * lytBaseURLEdit;
	TEdit * edtBaseURL;
	TClearEditButton * clrBaseURL;
	TLinkControlToField * LinkControlToField7;
	TLinkControlToField * LinkControlToField8;
	TSpeedButton * spdMaster;
	TImageList *imgListBox;
	TTabItem * tabExtEdit;
	TLayout * Layout13;
	TLayout *lytJSONIPAddress;
	TTabItem * tabLogin;
	TLayout * Layout15;
	TLayout * Layout17;
	TLayout * Layout18;
	TLayout * Layout19;
	TLayout * Layout20;
	TLayout * Layout21;
	TLabel * Label3;
	TLabel * Label4;
	TEdit * Edit1;
	TClearEditButton * ClearEditButton1;
	TEdit * Edit2;
	TClearEditButton * ClearEditButton3;
	TLayout * Layout22;
	TLabel * lblToken;
	TLinkControlToField * LinkControlToField10;
	TLinkControlToField * LinkControlToField11;
	TLinkPropertyToField * LinkPropertyToFieldText2;
	TLayout * Layout23;
	TLabel * lblLoginResponse;
	TLinkPropertyToField * LinkPropertyToFieldText3;
	TLayout * Layout24;
	TLabel * lblLoginResult;
	TLinkPropertyToField * LinkPropertyToFieldText4;
	TLayout * Layout25;
	TLabel * lblLoginErrorMsg;
	TLinkPropertyToField * LinkPropertyToFieldText5;
	TLayout * Layout26;
	TLabel * Label5;
	TLayout * Layout27;
	TLabel * Label6;
	TLayout * Layout28;
	TLabel * Label7;
	TLayout * Layout29;
	TLabel * Label8;
	TLayout * Layout30;
	TButton * btnSubmitLogin;
	TLayout * Layout31;
	TPanel * DetailPanel;
	TSpeedButton *SpeedButton1;
	TTabItem *tabQueues;
	TLayout *Layout32;
	TButton *btnGetQueues;
	TListBoxItem *lbQueues;
	TStringGrid *grdQue;
	TMemo *memLog;
	TLayout *Layout33;
	TToolBar *ToolBar2;
	TSpeedButton *btnClearLog;
	TListBoxItem *lbLog;
	TStringColumn *colQueue;
	TStringColumn *colMaxCalls;
	TStringColumn *colStrategy;
	TStringColumn *colCalls;
	TListBoxItem *lbExtensions;
	TListBoxItem *lbLogin;
	TListBoxItem *lbP2D;
	TListBoxItem *lbSettings;
	TLayout *Layout34;
	TGlyphColumn *colStatus;
	TStringColumn *colExtension;
	TStringColumn *colDevice;
	TStringColumn *colCallerID;
	TStringColumn *colOutboundCID;
	TImageList *imgExtStatus;
	TTabItem *tabIVR;
	TLayout *Layout35;
	TLayout *Layout36;
	TListBoxItem *lbIVR;
	TStringColumn *colHoldTime;
	TStringColumn *colTalkTime;
	TStringColumn *colCompleted;
	TStringColumn *colAbandoned;
	TStringColumn *colServiceLevel;
	TStringColumn *colServiceLevelPerf;
	TStringColumn *colWeight;
	TStringColumn *colDescription;
	TStringColumn *colMembers;
	TStringColumn *colAvailable;
	TButton *btnResetStats;
	TIdIOHandlerStack *IOHandlerStack;
	TIdConnectionIntercept *TCPIntercept;
	TIdTCPClient *TCPClient;
	TLabel *Label9;
	TLayout *Layout38;
	TLayout *Layout41;
	TLayout *Layout46;
	TLayout *Layout47;
	TButton *btnJSONConnect;
	TLayout *Layout44;
	TTMSFMXLED *ledJSONConnection;
	TLayout *Layout45;
	TLabel *lblBytesReceived;
	TLayout *Layout40;
	TLabel *Label10;
	TLayout *Layout48;
	TLabel *Label12;
	TLayout *Layout43;
	TSwitch *swLogin;
	TStringColumn *colState;
	TSwitch *swtLogOutput;
	TLayout *Layout49;
	TLayout *Layout50;
	TLabel *Label11;
	TLayout *Layout51;
	TLayout *Layout52;
	TLayout *Layout42;
	TAniIndicator *aniBusy;
	TListBoxItem *lbEditExtension;
	TSpeedButton *SpeedButton2;
	TPanel *Panel1;
	TExpander *Expander1;
	TLayout *Layout10;
	TLayout *Layout14;
	TLayout *Layout37;
	TEdit *Edit3;
	TLabel *Label2;
	TLayout *Layout39;
	TLayout *Layout53;
	TLabel *Label13;
	TLayout *Layout54;
	TLayout *Layout55;
	TLayout *Layout56;
	TLabel *Label14;
	TLayout *Layout57;
	TEdit *Edit5;
	TCalloutPanel *CalloutPanel1;
	TListBox *ListBox3;
	TSpeedButton *SpeedButton3;
	TLabel *lblSelected;
	TAniIndicator *aniQueues;
	TTimer *tmrKeepAlive;
	TAniIndicator *aniExtensions;
	TLayout *Layout58;
	TLabel *lblLoginTime;
	TLayout *Layout59;
	TLabel *Label16;
	TLayout *Layout60;
	TLabel *lblTimeStamp;
	TLayout *Layout61;
	TLabel *Label17;
	TLinkPropertyToField *LinkPropertyToFieldText6;
	TLayout *Layout62;
	TLabel *lblTenantName;
	TLayout *Layout63;
	TLabel *Label18;
	TLinkPropertyToField *LinkPropertyToFieldText7;
	TGlowEffect *gleToolBarLabel;
	TLayout *Layout64;
	TLabel *lblLoginStatus;
	TLayout *Layout65;
	TLabel *lblSettingsStatus;
	TLayout *Layout66;
	TLabel *lblAutoConnect;
	TLayout *Layout67;
	TLabel *lblLogOutput;
	TStringColumn *colCallData;
	TComboEdit *cbxHostAddress;
	TComboEdit *cbxHostPort;
	TStatusBar *stbIVR;
	TTMSFMXGrid *grdEIVR;
	TButton *Button1;
	TLabel *lblIVRText;
	TTabItem *tabSimulator;
	TLayout *Layout68;
	TLayout *Layout69;
	TTabControl *TabControl3;
	TTabItem *tabGetCustomer;
	TTabItem *tabTripStatus;
	TTabItem *tabBookOrder;
	TLayout *Layout70;
	TLabel *Label15;
	TEdit *edtHost;
	TLayout *Layout71;
	TButton *btnGCGetCustomer;
	TLabel *lblGCStatus;
	TStatusBar *sbGC;
	TListBoxItem *lbSimulator;
	TLabel *Label25;
	TLabel *Label26;
	TLabel *lblTSStatus;
	TLabel *Label22;
	TLabel *lblTSVehicleNo;
	TLayout *Layout72;
	TButton *btnGCGetTripStatus;
	TEdit *edtTSTripID;
	TLayout *Layout73;
	TButton *btnBookOrder;
	TButton *btnGCCancel;
	TTabItem *Output;
	TLayout *Layout74;
	TLabel *Label19;
	TEdit *edtGCCallerID;
	TLabel *Label20;
	TEdit *edtGCCalledID;
	TLabel *Label21;
	TEdit *edtGCUniqueID;
	TPresentedScrollBox *PresentedScrollBox1;
	TGroupBox *gbxPickup;
	TLabel *Label23;
	TLabel *Label27;
	TLabel *Label28;
	TLabel *Label29;
	TLabel *Label30;
	TLabel *Label31;
	TLabel *Label32;
	TLabel *Label33;
	TLabel *Label34;
	TLabel *Label35;
	TEdit *edtGCPName;
	TEdit *edtGCPStreetNo;
	TEdit *edtGCPStreet;
	TEdit *edtGCPCity;
	TEdit *edtGCPZip;
	TEdit *edtGCPBuilding;
	TEdit *edtGCPLatitude;
	TEdit *edtGCPLongitude;
	TEdit *edtGCPAddressID;
	TEdit *edtGCPApartment;
	TEdit *edtGCPAccessCode;
	TLabel *Label24;
	TGroupBox *gbxDropoff;
	TEdit *edtGCDAddressID;
	TLabel *Label36;
	TLabel *Label37;
	TLabel *Label38;
	TLabel *Label39;
	TLabel *Label40;
	TEdit *edtGCDLongitude;
	TEdit *edtGCDLatitude;
	TEdit *edtGCDBuilding;
	TEdit *edtGCDAccessCode;
	TLabel *Label41;
	TLabel *Label42;
	TEdit *edtGCDZip;
	TEdit *edtGCDCity;
	TLabel *Label43;
	TEdit *edtGCDStreet;
	TLabel *Label44;
	TEdit *edtGCDApartment;
	TEdit *edtGCDStreetNo;
	TLabel *Label45;
	TEdit *edtGCDName;
	TLabel *Label46;
	TGroupBox *gbxInfo;
	TLabel *Label47;
	TLabel *Label48;
	TLabel *Label49;
	TLabel *lblGCITimeStamp;
	TLabel *lblGCICommand;
	TLabel *lblGCIActionID;
	TLabel *lblGCIResult;
	TLabel *lblGCIResultCode;
	TLabel *lblGCIErrorMsg;
	TLabel *lblGCIVehicleNo;
	TLabel *lblGCIAccountNo;
	TLabel *lblGCIAccountName;
	TLabel *lblGCICustomerNo;
	TLabel *lblGCITripID;
	TLabel *lblGCIDispatchUniqueID;
	TLabel *lblGCIFleet;
	TLabel *lblGCIResponse;
	TLabel *lblGCILength;
	TGroupBox *GroupBox3;
	TComboBox *cbxAddresses;
	TLayout *Layout76;
	TLayout *Layout77;
	TLayout *Layout78;
	TLabel *Label70;
	TLabel *lblADAddressIndex;
	TLayout *Layout83;
	TLayout *Layout84;
	TLabel *Label78;
	TLabel *lblADAccessCode;
	TLayout *Layout85;
	TLayout *Layout86;
	TLabel *Label80;
	TLabel *lblADApartment;
	TLayout *Layout87;
	TLayout *Layout88;
	TLabel *Label82;
	TLabel *lblADBuildingName;
	TLayout *Layout89;
	TLayout *Layout90;
	TLabel *Label84;
	TLabel *lblADCityName;
	TLayout *Layout91;
	TLayout *Layout92;
	TLabel *Label86;
	TLabel *lblADStreetName;
	TLayout *Layout93;
	TLayout *Layout94;
	TLabel *Label88;
	TLabel *lblADStreetNumber;
	TLayout *Layout95;
	TLayout *Layout96;
	TLabel *Label90;
	TLabel *lblADZipCode;
	TLayout *Layout97;
	TLayout *Layout98;
	TLabel *Label96;
	TLabel *lblADMapCoordinates;
	TLayout *Layout99;
	TLayout *Layout100;
	TLabel *Label98;
	TLabel *lblADMapNumber;
	TLayout *Layout101;
	TLayout *Layout102;
	TLabel *Label100;
	TLabel *lblADDispatchZone;
	TLayout *Layout103;
	TLayout *Layout104;
	TLabel *Label102;
	TLabel *lblADLongitude;
	TLayout *Layout105;
	TLayout *Layout106;
	TLabel *Label104;
	TLabel *lblADLatitude;
	TLayout *Layout107;
	TLayout *Layout108;
	TLabel *Label106;
	TLabel *lblADName;
	TToolBar *ToolBar3;
	TButton *btnClearOutput;
	TGroupBox *GroupBox4;
	TLayout *Layout80;
	TLayout *Layout81;
	TLayout *Layout82;
	TLayout *Layout109;
	TLayout *Layout110;
	TLabel *Label73;
	TLabel *lblMIsDuplicate;
	TLayout *Layout111;
	TLayout *Layout112;
	TLabel *Label75;
	TLabel *lblMLastUpdate;
	TLayout *Layout113;
	TLayout *Layout114;
	TLabel *Label77;
	TLabel *lblMRebuiltTime;
	TLayout *Layout115;
	TLayout *Layout116;
	TLabel *Label81;
	TLabel *lblMIsBlocked;
	TLabel *Label85;
	TComboBox *cbxMRemarks;
	TLayout *Layout117;
	TLayout *Layout118;
	TLayout *Layout119;
	TLayout *Layout120;
	TLayout *Layout121;
	TLayout *Layout122;
	TLayout *Layout123;
	TLayout *Layout124;
	TLayout *Layout125;
	TLayout *Layout126;
	TLayout *Layout127;
	TLayout *Layout128;
	TLayout *Layout129;
	TLayout *Layout130;
	TLayout *Layout131;
	TLayout *Layout132;
	TLayout *Layout133;
	TLayout *Layout134;
	TLayout *Layout135;
	TLayout *Layout136;
	TLayout *Layout137;
	TLayout *Layout138;
	TLayout *Layout139;
	TLayout *Layout140;
	TLayout *Layout141;
	TLayout *Layout142;
	TLayout *Layout143;
	TLayout *Layout144;
	TLayout *Layout145;
	TLayout *Layout146;
	TLayout *Layout147;
	TLayout *Layout148;
	TLabel *Label50;
	TLabel *Label51;
	TLabel *Label52;
	TLabel *Label53;
	TLabel *Label54;
	TLabel *Label55;
	TLabel *Label56;
	TLabel *Label57;
	TLabel *Label58;
	TLabel *Label59;
	TLabel *Label60;
	TLabel *Label61;
	TLabel *Label62;
	TLabel *lblGCICellPhone;
	TPresentedScrollBox *PresentedScrollBox2;
	TButton *btnBOCancel;
	TGroupBox *GroupBox2;
	TLayout *Layout161;
	TLayout *Layout162;
	TLabel *Label99;
	TLabel *Label101;
	TComboBox *cbxBOAddresses;
	TLayout *Layout187;
	TLayout *Layout188;
	TLabel *Label130;
	TLabel *lblBOName;
	TLayout *Layout185;
	TLayout *Layout186;
	TLabel *Label128;
	TLabel *lblBOLatitude;
	TLayout *Layout183;
	TLayout *Layout184;
	TLabel *Label126;
	TLabel *lblBOLongitude;
	TLayout *Layout181;
	TLayout *Layout182;
	TLabel *Label124;
	TLabel *lblBODispatchZone;
	TLayout *Layout179;
	TLayout *Layout180;
	TLabel *Label122;
	TLabel *lblBOMapNo;
	TLayout *Layout177;
	TLayout *Layout178;
	TLabel *Label120;
	TLabel *lblBOMapCoords;
	TLayout *Layout175;
	TLayout *Layout176;
	TLabel *Label118;
	TLabel *lblBOZipCode;
	TLayout *Layout173;
	TLayout *Layout174;
	TLabel *Label116;
	TLabel *lblBOStreetNo;
	TLayout *Layout171;
	TLayout *Layout172;
	TLabel *Label114;
	TLabel *lblBOStreetName;
	TLayout *Layout169;
	TLayout *Layout170;
	TLabel *Label112;
	TLabel *lblBOCityName;
	TLayout *Layout167;
	TLayout *Layout168;
	TLabel *Label110;
	TLabel *lblBOBuildingName;
	TLayout *Layout165;
	TLayout *Layout166;
	TLabel *Label108;
	TLabel *lblBOApartment;
	TGroupBox *GroupBox1;
	TLayout *Layout75;
	TLayout *Layout149;
	TLabel *Label74;
	TLabel *lblBOFleet;
	TLayout *Layout150;
	TLayout *Layout151;
	TLabel *Label76;
	TLabel *lblBOAccountNo;
	TLayout *Layout152;
	TLayout *Layout153;
	TLabel *Label79;
	TDateEdit *deTripTime;
	TTimeEdit *teTripTime;
	TLayout *Layout154;
	TLayout *Layout155;
	TLabel *Label83;
	TLabel *lblBOPhoneNo;
	TLayout *Layout156;
	TLayout *Layout157;
	TLabel *Label87;
	TComboBox *cbxPassengers;
	TLayout *Layout158;
	TLayout *Layout159;
	TLabel *Label89;
	TRadioButton *radBOCash;
	TRadioButton *radBOCredit;
	TRadioButton *radBOAccount;
	TLayout *Layout160;
	TLayout *Layout189;
	TLabel *Label63;
	TLabel *Label64;
	TCheckBox *chkImmediate;
	TLayout *Layout190;
	TLayout *Layout191;
	TLabel *Label65;
	TLabel *lblBOCustomerNo;
	TLayout *Layout198;
	TLayout *Layout199;
	TLabel *Label92;
	TLabel *lblBOAccountName;
	TLayout *Layout163;
	TLayout *Layout164;
	TLabel *Label105;
	TLabel *lblBOAccessCode;
	TGroupBox *GroupBox5;
	TLayout *Layout192;
	TLayout *Layout193;
	TLabel *Label66;
	TLabel *lblBITimeStamp;
	TLayout *Layout194;
	TLayout *Layout195;
	TLabel *Label68;
	TLabel *lblBICommand;
	TLayout *Layout196;
	TLayout *Layout197;
	TLabel *Label71;
	TLabel *lblBIActionID;
	TLayout *Layout200;
	TLayout *Layout201;
	TLabel *Label91;
	TLabel *lblBIResult;
	TLayout *Layout202;
	TLayout *Layout203;
	TLabel *Label94;
	TLabel *lblBIResponse;
	TLayout *Layout204;
	TLayout *Layout205;
	TLabel *Label97;
	TLabel *lblBIErrorMsg;
	TLayout *Layout206;
	TLayout *Layout207;
	TLabel *Label107;
	TLabel *lblBIJobNo;
	TLayout *Layout208;
	TLayout *Layout209;
	TLabel *Label111;
	TLabel *lblBIDispatchZone;
	TLayout *Layout210;
	TLayout *Layout211;
	TLabel *Label115;
	TLabel *lblBIFareType;
	TLayout *Layout212;
	TLayout *Layout213;
	TLabel *Label119;
	TLabel *lblBIResponseTime;
	TLayout *Layout214;
	TLayout *Layout215;
	TLabel *Label123;
	TLabel *lblBIContentLength;
	TLayout *Layout216;
	TLayout *Layout217;
	TLabel *Label67;
	TLabel *lblBIResultCode;
	TLayout *Layout218;
	TLayout *Layout219;
	TLayout *Layout220;
	TLayout *Layout221;
	TLayout *Layout222;
	TLayout *Layout223;
	TLayout *Layout224;
	TLayout *Layout225;
	TLayout *Layout226;
	TLayout *Layout227;
	TLayout *Layout228;
	TLayout *Layout229;
	TLabel *lblTripIDStatus;
	TButton *btnGCCancelTrip;
	TLabel *lblDistanceETA;
	TWiggleTransitionEffect *WiggleTransitionEffect1;
	TFloatAnimation *FloatAnimation1;
	TButton *btnGCTimer;
	TLayout *Layout230;
	TNumberBox *nbxGCTimer;
	TTimer *tmrGetCust;
	TLayout *Layout232;
	TMemo *memDebug;
	TLayout *Layout233;
	TMemo *memGC;
	TSplitter *Splitter1;
	TButton *btnClearDebug;
	TLayout *Layout79;
	TLayout *Layout231;
	TLabel *lblMemGCLines;
	TIdTCPServer *TCPListener;
	TLayout *Layout234;
	TLayout *Layout235;
	TLabel *Label69;
	TLayout *Layout236;
	TEdit *Edit4;
	TButton *btnListen;
	TIdServerIOHandlerStack *IdServerIOHandlerStack1;
	TIdServerInterceptLogEvent *IdServerInterceptLogEvent1;
	TLayout *Layout237;
	TLabel *lblPackets;
	TTabItem *TabItem1;
	TLayout *Layout238;
	TLayout *Layout239;
	TLabel *Label72;
	TLayout *Layout240;
	TEdit *edtGoogleAPIKey;
	TLayout *Layout241;
	TLayout *Layout242;
	TLabel *Label93;
	TEdit *edtGeoAddress;
	TLayout *Layout243;
	TButton *btnGeocode;
	TMemo *memGoogle;
	TButton *btnClearGoogle;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall clrCallerIDClick(TObject *Sender);
	void __fastcall clrDriverPhoneClick(TObject *Sender);
	void __fastcall clrPassengerPhoneClick(TObject *Sender);
	void __fastcall clrBaseURLClick(TObject *Sender);
	void __fastcall btnRetrieveClick(TObject *Sender);
	void __fastcall clrCategoryClick(TObject *Sender);
	void __fastcall grdExtensionsDrawColumnCell(TObject *Sender, TCanvas * const Canvas, TColumn * const Column,
		 const TRectF &Bounds, const int Row, const TValue &Value, const TGridDrawStates State);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall TabControl1Change(TObject *Sender);
	void __fastcall lbP2DClick(TObject *Sender);
	void __fastcall lbLogClick(TObject *Sender);
	void __fastcall lbExtensionsClick(TObject *Sender);
	void __fastcall lbSettingsClick(TObject *Sender);
	void __fastcall lbLoginClick(TObject *Sender);
	void __fastcall btnSubmitLoginClick(TObject *Sender);
	void __fastcall btnGetQueuesClick(TObject *Sender);
	void __fastcall lbQueuesClick(TObject *Sender);
	void __fastcall btnClearLogClick(TObject *Sender);
	void __fastcall lbIVRClick(TObject *Sender);
	void __fastcall btnResetStatsClick(TObject *Sender);
	void __fastcall IOHandlerStackStatus(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText);
	void __fastcall TCPInterceptConnect(TIdConnectionIntercept *ASender);
	void __fastcall TCPInterceptDisconnect(TIdConnectionIntercept *ASender);
	void __fastcall TCPInterceptReceive(TIdConnectionIntercept *ASender, TIdBytes &ABuffer);
	void __fastcall TCPInterceptSend(TIdConnectionIntercept *ASender, TIdBytes &ABuffer);
	void __fastcall TCPClientConnected(TObject *Sender);
	void __fastcall TCPClientDisconnected(TObject *Sender);
	void __fastcall TCPClientStatus(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText);
	void __fastcall TCPClientWork(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCount);
	void __fastcall TCPClientWorkBegin(TObject *ASender, TWorkMode AWorkMode, __int64 AWorkCountMax);
	void __fastcall TCPClientWorkEnd(TObject *ASender, TWorkMode AWorkMode);
	void __fastcall OnThreadTerminate(TObject *Sender);
	void __fastcall btnJSONConnectClick(TObject *Sender);
	void __fastcall Log(UnicodeString asLog);
	void __fastcall GCLog(UnicodeString asOut);
	void __fastcall GoogleLog(UnicodeString asLog);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	bool __fastcall GetColumnByName(TStringGrid *Grd, AnsiString ColumnName, int &ColumnIndex);
	void __fastcall GetExtensionStates();
	void __fastcall lbEditExtensionClick(TObject *Sender);
	void __fastcall ListView1PullRefresh(TObject *Sender);
	void __fastcall expUserManagerClick(TObject *Sender);
	void __fastcall MultiView1Shown(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall CalloutPanel1Click(TObject *Sender);
	void __fastcall ListBox3ChangeCheck(TObject *Sender);
	void __fastcall SpeedButton3Click(TObject *Sender);
	void __fastcall SpeedButton2Click(TObject *Sender);
	void __fastcall tmrKeepAliveTimer(TObject *Sender);
	void __fastcall MultiView1Hidden(TObject *Sender);
	void __fastcall SizeGridColumns(TStringGrid *Grid);
	UnicodeString __fastcall GetCurrentDateTimeMS(UnicodeString FmtString);
	void __fastcall OnConnectionStatusChange(TObject *Sender);
	void __fastcall swtLogOutputSwitch(TObject *Sender);
	void __fastcall swLoginSwitch(TObject *Sender);
	void __fastcall FillIVRGrid(UnicodeString uJSONText);
	void __fastcall grdEIVRResize(TObject *Sender);
	void __fastcall grdEIVRCellSwitchClick(TObject *Sender, int ACol, int ARow, TFmxObject *Cell);
	void __fastcall grdEIVRColumnSize(TObject *Sender, int ACol, float &NewWidth);
	void __fastcall grdEIVRColumnSized(TObject *Sender, int ACol, float NewWidth);
	void __fastcall btnGCGetCustomerClick(TObject *Sender);
	void __fastcall lbSimulatorClick(TObject *Sender);
	void __fastcall btnGCCancelClick(TObject *Sender);
	void __fastcall edtTSTripIDKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar, TShiftState Shift);
	void __fastcall cbxAddressesChange(TObject *Sender);
	UnicodeString __fastcall GetNonNull(TJSONObject *oJObj, UnicodeString FldName, UnicodeString DefVal);
	void __fastcall btnClearOutputClick(TObject *Sender);
	void __fastcall cbxBOAddressesChange(TObject *Sender);
	void __fastcall chkImmediateChange(TObject *Sender);
	void __fastcall btnBookOrderClick(TObject *Sender);
	void __fastcall btnGCTimerClick(TObject *Sender);
	void __fastcall nbxGCTimerChange(TObject *Sender);
	void __fastcall memDebugDblClick(TObject *Sender);
	void __fastcall memGCChange(TObject *Sender);
	void __fastcall TCPListenerConnect(TIdContext *AContext);
	void __fastcall TCPListenerDisconnect(TIdContext *AContext);
	void __fastcall OnDisconnect(TObject *Sender);
	void __fastcall TCPListenerException(TIdContext *AContext, Exception *AException);
	void __fastcall TCPListenerListenException(TIdListenerThread *AThread, Exception *AException);
	void __fastcall TCPListenerStatus(TObject *ASender, const TIdStatus AStatus, const UnicodeString AStatusText);
	void __fastcall btnListenClick(TObject *Sender);
	void __fastcall TCPListenerExecute(TIdContext *AContext);
	void __fastcall IdServerInterceptLogEvent1LogString(TIdServerInterceptLogEvent *ASender, const UnicodeString AText);
	void __fastcall btnGeocodeClick(TObject *Sender);
	void __fastcall btnClearGoogleClick(TObject *Sender);

private: // User declarations
	bool IsLoggedIn;
	String Token;
	TJSONObject * JResponseObj;

	void __fastcall SetSwitch(TTMSFMXGrid * Grd, TJSONObject * JObj, AnsiString FieldName, int ACol, int ARow);

public: // User declarations
	__fastcall TfrmRestClient(TComponent* Owner);
	void __fastcall UpdateClearedText(TClearEditButton *ClearButton, TEdit *EditField);
	};

// ---------------------------------------------------------------------------
extern PACKAGE TfrmRestClient *frmRestClient;
// ---------------------------------------------------------------------------
#endif
