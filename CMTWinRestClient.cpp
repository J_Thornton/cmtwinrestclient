// ---------------------------------------------------------------------------

#include <fmx.h>
#ifdef _WIN32
#include <tchar.h>
#endif
#pragma hdrstop
#include <System.StartUpCopy.hpp>
// ---------------------------------------------------------------------------
USEFORM("CMTRestClientDataModule.cpp", dmRestClient); /* TDataModule: File Type */
USEFORM("CMTWinRestClientForm.cpp", frmRestClient);

// ---------------------------------------------------------------------------
extern "C" int FMXmain()
	{
	try
		{
		Application->Initialize();
		Application->CreateForm(__classid(TfrmRestClient), &frmRestClient);
		Application->CreateForm(__classid(TdmRestClient), &dmRestClient);
		Application->Run();
		}
	catch (Exception &exception)
		{
		Application->ShowException(&exception);
		}
	catch (...)
		{
		try
			{
			throw Exception("");
			}
		catch (Exception &exception)
			{
			Application->ShowException(&exception);
			}
		}
	return 0;
	}
// ---------------------------------------------------------------------------
